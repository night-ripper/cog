fn main() -> std::io::Result<()>
{
	println!("cargo:rerun-if-changed={}", "build.rs");
	
	let _os_cmd_prefix = if std::env::var_os("CARGO_CFG_TARGET_OS").unwrap() == "windows"
	{
		"x86_64-w64-mingw32-"
	}
	else
	{
		""
	};
	
	// lld is required for -flto
	println!("cargo:rustc-link-arg=-fuse-ld={}", "lld");
	println!("cargo:rustc-link-lib={}", "jemalloc");
	
	for entry in std::fs::read_dir("src/main/adoc")?
	{
		println!("cargo:rerun-if-changed={}", entry?.path().to_str().unwrap());
	}
	
	std::process::Command::new("asciidoctor").args([
		"src/main/adoc/manpage.adoc", "-b", "manpage", "-D", "target/doc",
	].iter()).output()?;
	std::process::Command::new("asciidoctor").args([
		"src/main/adoc/README.adoc", "-D", "target/doc",
	].iter()).output()?;
	
	let cc = std::env::var("CC").unwrap_or("clang".into());
	let color = match cc.as_str()
	{
		"gcc" => "-fdiagnostics-color=always",
		"clang" => "-fcolor-diagnostics",
		_ => ""
	};
	
	let cflags = std::env::var("CFLAGS").unwrap_or("-g -flto".into())
		+ " -std=c99"
		+ " -Wall"
		+ " -Wextra"
		+ " -Wpedantic"
		+ " -Wconversion"
		+ " -Werror=implicit-function-declaration"
		+ " -Wno-varargs"
		+ " " + color
	;
	// let ldflags = std::env::var("LDFLAGS").unwrap_or("-flto".into());
	// let ldlibs = std::env::var("LDLIBS").unwrap_or_default() + " -lruby";
	
	std::fs::create_dir_all("target/c")?;
	for entry in std::fs::read_dir("src/main/c")?
	{
		println!("cargo:rerun-if-changed={}", entry?.path().to_str().unwrap());
	}
	
	for source_file in ["raven", /*"raven_mruby"*/]
	{
		let output = std::process::Command::new("sh").arg("-ex").arg("-c").arg(format!(
			"{cc} {cflags} -c -o target/c/{source_file}.o src/main/c/{source_file}.c"
		)).output()?;
		
		if ! output.status.success()
		{
			println!("{}", std::str::from_utf8(&output.stdout).unwrap());
			eprintln!("{}", std::str::from_utf8(&output.stderr).unwrap());
			return Err(std::io::ErrorKind::Other.into());
		}
		
		if ! output.stderr.is_empty()
		{
			for line in std::str::from_utf8(&output.stderr).unwrap().lines()
			{
				println!("cargo:warning={}", line);
			}
		}
	}
	
	/*
	 * NOTE
	{
		let output = std::process::Command::new("sh").arg("-ex").arg("-c").arg(format!(
			"{cc} {cflags} {ldflags} -fpic -shared -o target/c/ld_preload.so src/main/c/ld_preload.c"
		)).output()?;
		
		if ! output.status.success()
		{
			println!("{}", std::str::from_utf8(&output.stdout).unwrap());
			eprintln!("{}", std::str::from_utf8(&output.stderr).unwrap());
			return Err(std::io::ErrorKind::Other.into());
		}
		
		if ! output.stderr.is_empty()
		{
			for line in std::str::from_utf8(&output.stderr).unwrap().lines()
			{
				println!("cargo:warning={}", line);
			}
		}
	}
	*/
	
	if ! std::process::Command::new("ar").args(["-rcs", "target/c/libRaven.a",
		"target/c/raven.o",
		// "target/c/raven_mruby.o",
		].iter()).spawn()?.wait()?.success()
	{
		return Err(std::io::ErrorKind::Other.into());
	}
	
	println!("cargo:rustc-link-search=native={}", "target/c");
	
	Ok(())
}

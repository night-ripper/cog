include Raven

module Raven
module Tools
class Lmao < Target
end
end
module Cxx
extend self
CXX = env("CXX", "c++")
CPPFLAGS = env("CPPFLAGS", "")
CXXFLAGS = env("CXXFLAGS", "-g") << " -std=c++2a -Isrc"
LDFLAGS = env("LDFLAGS", "")
LDLIBS = env("LDLIBS", "")
@arch = %x(#{CXX} -dumpmachine)[.. -2]
def object_file(name, source_files)
	name = "target/#{@arch}/object_files/" + name
	dependencies = []
	for source_file in source_files do
		get_header_dependencies(source_file, dependencies)
	end
	target name, source_files + dependencies, exec: ->{"
		#{CXX} -c -o #{@target} #{CPPFLAGS} #{CXXFLAGS} #{@sources[0]}
	"}
end
def object_file_single(source_file)
	object_filename = source_file["src/".length ..].gsub("/", ".")
	object_filename = object_filename[.. object_filename.rindex(".")] + "o"
	object_file object_filename, [source_file]
end
def executable(name, object_files)
	name = "target/#{@arch}/bin/" + name
	target name, object_files, exec: ->{"
		#{CXX} -o #{@target} #{LDFLAGS} #{LDLIBS} #{@sources}
	"}
end
end
end

# Tools::Lmao.new

target :manpages do
	for manpage in ["java_remove_annotations.7", "java_remove_imports.7"] do
		@sources << target("target/manpages/#{manpage}", ["manpages/#{manpage}.adoc"], exec: ->{"
			asciidoctor -b manpage -D target/manpages #{@sources}
		"}, implicit_directories: false)
	end
end

jurand_exe = Cxx::executable("jurand", [Cxx::object_file_single("src/jurand.cpp")])
target :compile, [jurand_exe]
jurand_test = Cxx::executable("jurand_test", [Cxx::object_file_single("src/jurand_test.cpp")])
puts "ôôôô"
target :"test-compile", [jurand_test]
puts "ôôôô"
target :test, [jurand_test, jurand_exe], exec: ->{"./test.sh #{@sources}"}
puts "ôôôô"

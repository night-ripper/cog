CXX = (ENV["CXX"] or "g++")
CXXFLAGS = (ENV["CXXFLAGS"] or "") + " -std=c++2a -Isrc -Wall -Wextra -Wpedantic"
LDFLAGS = (ENV["LDFLAGS"] or "")
LDLIBS = (ENV["LDLIBS"] or "")

def dep(*args)
	puts args, "AAAA"
end

module Raven
	@@targets = {"build" => true, "coverage" => true}
	
	@@aaa = 5
	
	def targets
		@@targets
	end
	
	class Rule
		def initialize(*args, implicit_directories)
			puts "LOCAL"
			@t = args
			puts implicit_directories
			@s = []
			@exec = ->(*args) do
				puts "#{args}"
			end
			@export = ->(*args) do
				puts "#{args}"
			end
			@sh = ->(*args) do
				puts "#{args}"
			end
			@env = Hash.new("")
			@env[:CXX] = "g++"
		end
	end
end

module Raven
	def rule(pattern, implicit_directories: true, &block)
		result = Rule.new(pattern, implicit_directories)
		result.instance_exec(&block)
		puts "#block #{block.object_id}"
		result
	end
end

Raven.extend(Raven)
Raven.private_constant(:Rule)

include Raven
# extend Raven
# using Raven

puts targets

# Raven::rule

rule :manpages do
	l = ->() do
		@s << "a"
	end
	puts @s
	@s << ->do
	end
	# for a in [1,2,3] do
	3.times do |a|
		@s << (rule a.to_s, &l)
	end
end

r = rule %r|target/object_files/(.*)\.o|, implicit_directories: false do
	@s << "src/#{@t[1]}.cpp" << "a"
	@env["CXX"] += " aaa"
	@exec[@env["CXX"], CXXFLAGS, "-c", "-o", @t[0], @s[0]]
	puts @s
end

Raven.rule %r|target/object_files/(.*)\.o| do
	puts @s
	# puts targets["build"]
	# puts targets["builda"]
end

puts targets["build"]
puts targets["builda"]
targets["a"] = "a"
puts targets.object_id

def target(*args, c: True, **kwargs, &block)
	puts c
end

target "a", :c => "a" do
	target "A"
end

# (rule >> "build" >> "asd" << "compile") {puts "echo #{@sources.join(",")}"}

# puts rule.targets
# puts rule.sources

=begin
rule >> "build" << "compile" == {|targets, sources| [
	"", ""
]}
=end

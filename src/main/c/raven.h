#define _GNU_SOURCE
#include <errno.h>
#include <unistd.h>

struct IO_Result
{
	ssize_t len;
	int errnum;
	// const char* error;
};

struct String_view
{
	const char* data;
	unsigned long len;
};

struct String
{
	const char* data;
	unsigned long len;
};

struct Script_info
{
	const char* libname;
	struct String_view script_filename;
	const char* targets;
	void* shell_param;
	void* rule_data;
	void* rule_ids;
	void* string_repository;
	unsigned long num_targets;
	unsigned char implicit_directories;
	unsigned char use_wordexp;
};

struct Script_info* static_script_info = NULL;

int Raven_pipe(int fds[2])
{
	if (pipe(fds) == -1)
	{
		return errno;
	}
	
	return 0;
}

int Raven_pipe2(int fds[2], int flags)
{
	if (pipe2(fds, flags) == -1)
	{
		return errno;
	}
	
	return 0;
}

struct IO_Result Raven_read(int fd, void *buf, size_t nbytes)
{
	struct IO_Result result;
	result.len = read(fd, buf, nbytes);
	result.errnum = 0;
	// result.error = NULL;
	if (result.len == -1)
	{
		result.errnum = errno;
		// result.error = strerror(result.errnum);
	}
	return result;
}

struct IO_Result Raven_write(int fd, const void *buf, size_t n)
{
	struct IO_Result result;
	result.len = write(fd, buf, n);
	result.errnum = 0;
	// result.error = NULL;
	if (result.len == -1)
	{
		result.errnum = errno;
		// result.error = strerror(result.errnum);
	}
	return result;
}

int Raven_fsync(int fd)
{
	if (fsync(fd) == -1)
	{
		return errno;
	}
	
	return 0;
}

extern size_t Raven_rst_new_target(size_t object_id, char implicit_directories);
extern void Raven_rst_target_target_string(size_t id, const char* name, long len);
extern void Raven_rst_target_target_symbol(size_t id, const char* name);
extern void Raven_rst_target_source_string(size_t id, const char* name, long len);
extern void Raven_rst_target_source_symbol(size_t id, const char* name);
extern void Raven_rst_target_source_target(size_t id, size_t source_object_id);
extern void Raven_rst_target_env(size_t id, const char* key, long key_len, const char* value, long value_len);
extern void Raven_rst_target_exec_array(size_t id, const char* arg, long arg_len);
extern void Raven_rst_target_exec_string(size_t id, const char* string, long len);

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <dlfcn.h>
#include <errno.h>

static int read_fd = 0;
static int write_fd = 0;
static int(*libc_open)(const char *pathname, int flags, ...) = NULL;
static int(*libc_open64)(const char *pathname, int flags, ...) = NULL;
static int(*libc_openat)(int dirfd, const char *pathname, int flags, ...) = NULL;

__attribute__((constructor))
static void construct(void)
{
	sscanf(getenv("RAVEN_PRELOAD_DATA"), "%d %d", &read_fd, &write_fd);
	libc_open = (int(*)(const char *, int, ...)) dlsym(RTLD_NEXT, "open");
	libc_open64 = (int(*)(const char *, int, ...)) dlsym(RTLD_NEXT, "open64");
	libc_openat = (int(*)(int, const char *, int, ...)) dlsym(RTLD_NEXT, "openat");
}

// __attribute__((destructor))
// static void destroy(void)
// {
// }

int open(const char *pathname, int flags, ...)
{
	printf("open called: %s\n", pathname);
	ssize_t ioresult;
	ioresult = write(write_fd, pathname, strlen(pathname));
	printf("write(%d, %s) -> %ld\n", write_fd, pathname, ioresult);
	if (ioresult == -1)
	{
		puts(strerror(errno));
	}
	char buf = 0;
	ioresult = read(read_fd, &buf, 1);
	printf("read(%d) -> %ld (%c)\n", read_fd, ioresult, buf);
	if (ioresult == -1)
	{
		puts(strerror(errno));
	}
	
	int result;
	if (flags & O_CREAT)
	{
		va_list args;
		va_start(args, 1);
		mode_t mode = va_arg(args, mode_t);
		va_end(args);
		result = libc_open(pathname, flags, mode);
	}
	else
	{
		result = libc_open(pathname, flags);
	}
	return result;
}

int open64(const char *pathname, int flags, ...)
{
	va_list args;
	va_start(args, 1);
	mode_t mode = va_arg(args, mode_t);
	va_end(args);
	
	printf("write: %lu, %s\n", write(write_fd, pathname, strlen(pathname)), pathname);
	// char buf = 0;
	// printf("read: %lu\n", read(read_fd, &buf, 1));
	int result = libc_open64(pathname, flags, mode);
	printf("libc_open64: %d\n", result);
	return result;
}

int openat(int dirfd, const char *pathname, int flags, ...)
{
	va_list args;
	va_start(args, 1);
	mode_t mode = va_arg(args, mode_t);
	va_end(args);
	
	int result = libc_openat(dirfd, pathname, flags, mode);
	printf("libc_openat: %d\n", result);
	return result;
}

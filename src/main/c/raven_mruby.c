#include "raven.h"

#include <string.h>

#include <mruby.h>
#include <mruby/compile.h>
#include <mruby/hash.h>
#include <mruby/variable.h>

/*
static struct RClass* static_raven_module = NULL;
static mrb_value static_targets_parent_sources = {MRB_Qundef};
static struct RClass* static_environment_variable_class = NULL;
static struct RClass* static_target_class = NULL;
static size_t static_targets_parent_id = (size_t) -1;
*/

int Raven_main(void)
{
	int exitcode = 0;
	mrb_state* mrb = mrb_open();
	FILE* script_file = fopen(static_script_info->script_filename.data, "r");
	
	if (script_file == NULL)
	{
		fprintf(stderr, "raven: error opening %s: %s\n", static_script_info->script_filename.data, strerror(errno));
		exitcode = 1;
	}
	else
	{
		/*
		static_raven_module = mrb_define_module(mrb, static_script_info->libname);
		static_environment_variable_class = mrb_define_class_under(mrb, static_raven_module, "Environment_variable", (RClass*)&RString);
		static_target_class = rb_define_class_under(static_raven_module, "Target", rb_cObject);
		
		mrb_value raven_targets = mrb_hash_new_capa(mrb, (long) static_script_info->num_targets);
		mrb_iv_set(mrb, static_raven_module, mrb_intern(mrb, "@targets"), raven_targets);
		{
			size_t beg = 0;
			for (unsigned long i = 0; i != static_script_info->num_targets; ++i)
			{
				size_t len = 0;
				while (static_script_info->targets[beg + len] != '\0')
				{
					++len;
				}
				rb_hash_aset(raven_targets, ID2SYM(rb_intern2(static_script_info->targets + beg, (long) len)), Qtrue);
				beg += len + 1;
			}
		}
		*/
		// TODO ...
		mrb_value result = mrb_load_file(mrb, script_file);
		printf("%lu\n", result.w);
		
		fclose(script_file);
	}
	
	mrb_close(mrb);
	return exitcode;
}

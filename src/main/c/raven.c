#include "raven.h"

#include <ruby.h>
#include <stdio.h>
#include <wordexp.h>

static VALUE static_raven_module = Qundef;
static VALUE static_raven_cxx_module = Qundef;
static VALUE static_environment_variable_class = Qundef;
static VALUE static_target_class = Qundef;
static VALUE static_spaced_array_class = Qundef;

static size_t read_backslash(const char* string, size_t pos, VALUE result, const char** err)
{
	pos += 1;
	if (string[pos] == '\0')
	{
		*err = "unterminated backslash";
		return (size_t) -1;
	}
	pos += 1;
	rb_str_cat(result, string + pos - 1, 1);
	return pos;
}

static size_t read_single_quoted(const char* string, size_t pos, VALUE result, const char** err)
{
	pos += 1;
	long old_pos = (long) pos;
	while (string[pos] != '\0')
	{
		pos += 1;
		if (string[pos - 1] == '\'')
		{
			rb_str_cat(result, string + old_pos, (long) pos - old_pos - 1);
			return pos;
		}
	}
	
	*err = "unterminated single quote";
	return (size_t) -1;
}

static size_t read_double_quoted(const char* string, size_t pos, VALUE result, const char** err)
{
	pos += 1;
	long old_pos = (long) pos;
	while (string[pos] != '\0')
	{
		if (string[pos] == '\"')
		{
			pos += 1;
			rb_str_cat(result, string + old_pos, (long) pos - old_pos - 1);
			return pos;
		}
		else if (string[pos] == '\\')
		{
			rb_str_cat(result, string + old_pos, (long) pos - old_pos);
			pos = read_backslash(string, pos, result, err);
			if (pos == (size_t) -1)
			{
				return (size_t) -1;
			}
			old_pos = (long) pos;
		}
		else
		{
			pos += 1;
		}
	}
	
	*err = "unterminated double quote";
	return (size_t) -1;
}

static VALUE expand(const char* string, const char** err)
{
	VALUE result_array = rb_ary_new();
	size_t pos = 0;
	
	while (string[pos] != '\0')
	{
		if (isspace(string[pos]))
		{
			pos += 1;
			continue;
		}
		
		VALUE writer = rb_str_new("", 0);
		rb_ary_push(result_array, writer);
		const size_t orig_pos = pos;
		long old_pos = (long) pos;
		while (string[pos] != '\0' && ! isspace(string[pos]))
		{
			size_t prev_pos = pos;
			if (string[pos] == '\'')
			{
				rb_str_cat(writer, string + old_pos, (long) pos - old_pos);
				pos = read_single_quoted(string, pos, writer, err);
				old_pos = (long) pos;
				if (pos == (size_t) -1)
				{
					return Qundef;
				}
				if (prev_pos + 2 == pos && pos - orig_pos == 2)
				{
					break;
				}
				continue;
			}
			
			if (string[pos] == '\"')
			{
				rb_str_cat(writer, string + old_pos, (long) pos - old_pos);
				pos = read_double_quoted(string, pos, writer, err);
				old_pos = (long) pos;
				if (pos == (size_t) -1)
				{
					return Qundef;
				}
				if (prev_pos + 2 == pos && pos - orig_pos == 2)
				{
					break;
				}
				continue;
			}
			
			if (string[pos] == '\\')
			{
				rb_str_cat(writer, string + old_pos, (long) pos - old_pos);
				pos = read_backslash(string, pos, writer, err);
				old_pos = (long) pos;
				if (pos == (size_t) -1)
				{
					return Qundef;
				}
			}
			else
			{
				pos += 1;
			}
		}
		
		rb_str_cat(writer, string + old_pos, (long) pos - old_pos);
	}
	
	return result_array;
}

extern void Raven_rst_shell(const char* string, long len);

static VALUE Raven_function_shell(int argc, VALUE *argv, VALUE obj)
{
	(void) obj;
	VALUE command;
	VALUE args;
	int scan_result = rb_scan_args(argc, argv, "1*", &command, &args);
	(void) scan_result;
	
	VALUE shell_array = rb_ary_new_capa(argc + 1);
	
	const enum ruby_value_type command_type = rb_type(command);
	if (command_type != T_STRING)
	{
		rb_raise(rb_eTypeError, "invalid type of the first argument: %d", command_type);
	}
	
	rb_ary_push(shell_array, rb_str_freeze(command));
	Raven_rst_shell(RSTRING_PTR(command), RSTRING_LEN(command));
	
	long args_len = rb_array_len(args);
	for (long i = 0; i != args_len; ++i)
	{
		VALUE entry = rb_ary_entry(args, i);
		const enum ruby_value_type entry_type = rb_type(entry);
		switch (entry_type)
		{
		case T_STRING:
			rb_ary_push(shell_array, rb_str_freeze(entry));
			Raven_rst_shell(RSTRING_PTR(entry), RSTRING_LEN(entry));
			break;
		default:
			rb_raise(rb_eTypeError, "invalid type of the argument #%ld: %d", i + 1, entry_type);
		}
	}
	
	rb_ivar_set(static_raven_module, rb_intern("@shell"), rb_obj_freeze(shell_array));
	
	return Qnil;
}

static VALUE Raven_function_implicit_directories(VALUE obj, VALUE value)
{
	(void) obj;
	const enum ruby_value_type command_type = rb_type(value);
	switch (command_type)
	{
	case T_TRUE:
		static_script_info->implicit_directories = 1;
		break;
	case T_FALSE:
		static_script_info->implicit_directories = 0;
		break;
	default:
		rb_raise(rb_eTypeError,
			"invalid type of the first argument, expected `true` or `false`, got: %d",
			command_type
		);
	}
	
	rb_ivar_set(static_raven_module, rb_intern("@implicit_directories"), value);
	
	return Qnil;
}

static VALUE Raven_function_target_to_array(VALUE obj)
{
	return rb_ivar_get(obj, rb_intern("@targets"));
}

static int env_iter_callback(VALUE key, VALUE value, VALUE arg)
{
	const enum ruby_value_type key_type = rb_type(key);
	if (key_type != T_STRING)
	{
		rb_raise(rb_eTypeError, "env invalid type for key: %d", key_type);
	}
	
	const enum ruby_value_type value_type = rb_type(value);
	if (key_type != T_STRING)
	{
		rb_raise(rb_eTypeError, "env invalid type for value: %d", value_type);
	}
	
	Raven_rst_target_env(FIX2ULONG(arg), RSTRING_PTR(key), RSTRING_LEN(key), RSTRING_PTR(value), RSTRING_LEN(value));
	
	return ST_CONTINUE;
}

/*
 * target(targets, sources = nil, implicit_directories: true, env: {}, exec: nil, &block)
 */
static VALUE Raven_function_target(int argc, VALUE *argv, VALUE obj)
{
	(void) obj;
	VALUE targets;
	VALUE sources = Qnil;
	VALUE keywords;
	VALUE block;
	int scan_result = rb_scan_args(argc, argv, "11:&", &targets, &sources, &keywords, &block);
	(void) scan_result;
	
	ID table[] =
	{
		rb_intern("env"),
		rb_intern("exec"),
		rb_intern("implicit_directories"),
	};
	VALUE kw_values[sizeof(table) / sizeof(*table)];
	rb_get_kwargs(keywords, table, 0, sizeof(table) / sizeof(*table), kw_values);
	
	VALUE result = rb_funcall(static_target_class, rb_intern("new"), 0);
	size_t id = Raven_rst_new_target(result, kw_values[2] == Qtrue ? 1 : kw_values[2] == Qfalse ? 0 : -1);
	VALUE target_array;
	
	const enum ruby_value_type targets_type = rb_type(targets);
	switch (targets_type)
	{
	case T_ARRAY:
	{
		long targets_len = rb_array_len(targets);
		target_array = rb_ary_new_capa(targets_len);
		if (targets_len == 0)
		{
			rb_raise(rb_eArgError, "targets: empty array");
		}
		for (long i = 0; i != targets_len; ++i)
		{
			VALUE target = rb_ary_entry(targets, i);
			const enum ruby_value_type target_type = rb_type(target);
			switch (target_type)
			{
			case T_STRING:
			{
				const char* name = RSTRING_PTR(target);
				long len = RSTRING_LEN(target);
				Raven_rst_target_target_string(id, name, len);
				rb_ary_push(target_array, rb_obj_freeze(target));
				break;
			}
			case T_SYMBOL:
			{
				const char* name = rb_id2name(SYM2ID(target));
				Raven_rst_target_target_symbol(id, name);
				rb_ary_push(target_array, target);
				break;
			}
			default:
			{
				rb_raise(rb_eTypeError, "invalid type: %d", target_type);
				break;
			}
			}
		}
		break;
	}
	case T_STRING:
	{
		const char* name = RSTRING_PTR(targets);
		long len = RSTRING_LEN(targets);
		Raven_rst_target_target_string(id, name, len);
		target_array = rb_ary_new_capa(1);
		rb_ary_push(target_array, rb_obj_freeze(targets));
		break;
	}
	case T_SYMBOL:
	{
		const char* name = rb_id2name(SYM2ID(targets));
		Raven_rst_target_target_symbol(id, name);
		target_array = rb_ary_new_capa(1);
		rb_ary_push(target_array, targets);
		break;
	}
	default:
	{
		rb_raise(rb_eTypeError, "invalid type: %d", targets_type);
		break;
	}
	}
	
	rb_ivar_set(result, rb_intern("@targets"), rb_funcall(static_spaced_array_class, rb_intern("new"), 1, rb_obj_freeze(target_array)));
	rb_ivar_set(result, rb_intern("@target"), rb_obj_freeze(rb_ary_entry(target_array, 0)));
	
	VALUE source_array = rb_obj_dup(rb_funcall(sources, rb_intern("to_a"), 0));
	source_array = rb_funcall(static_spaced_array_class, rb_intern("new"), 1, source_array);
	rb_ivar_set(result, rb_intern("@sources"), source_array);
	rb_ivar_set(result, rb_intern("@env"), kw_values[0]);
	rb_ivar_set(result, rb_intern("@implicit_directories"), kw_values[2]);
	
	if (rb_obj_is_proc(kw_values[1]) == Qtrue)
	{
		kw_values[1] = rb_funcall_with_block(result, rb_intern("instance_exec"), 0, NULL, kw_values[1]);
	}
	rb_ivar_set(result, rb_intern("@exec"), kw_values[1]);
	
	if (rb_block_given_p())
	{
		rb_obj_instance_exec(1, &block, result);
	}
	
	long sources_len = rb_array_len(source_array);
	for (long i = 0; i != sources_len; ++i)
	{
		VALUE source = rb_ary_entry(source_array, i);
		const enum ruby_value_type source_type = rb_type(source);
		switch (source_type)
		{
		case T_OBJECT:
		{
			Raven_rst_target_source_target(id, source);
			break;
		}
		case T_STRING:
		{
			Raven_rst_target_source_string(id, RSTRING_PTR(source), RSTRING_LEN(source));
			break;
		}
		case T_SYMBOL:
		{
			Raven_rst_target_source_symbol(id, rb_id2name(SYM2ID(source)));
			break;
		}
		default:
		{
			rb_raise(rb_eTypeError, "invalid type: %d", source_type);
			break;
		}
		}
	}
	
	const enum ruby_value_type env_type = rb_type(kw_values[0]);
	if (env_type != T_UNDEF)
	{
		if (env_type != T_HASH)
		{
			rb_raise(rb_eTypeError, "invalid type of the env argument: %d", env_type);
		}
		
		rb_hash_foreach(kw_values[0], env_iter_callback, RB_LONG2FIX(id));
	}
	
	kw_values[1] = rb_ivar_get(result, rb_intern("@exec"));
	enum ruby_value_type exec_type = rb_type(kw_values[1]);
	switch (exec_type)
	{
	case T_UNDEF:
	{
		break;
	}
	case T_ARRAY:
	{
		long exec_len = rb_array_len(kw_values[1]);
		if (exec_len == 0)
		{
			rb_raise(rb_eArgError, "exec: empty array");
		}
		for (long i = 0; i != exec_len; ++i)
		{
			VALUE exec_arg = rb_ary_entry(kw_values[1], i);
			const enum ruby_value_type exec_entry_type = rb_type(exec_arg);
			if (exec_entry_type != T_STRING)
			{
				rb_raise(rb_eTypeError, "invalid type of the exec array entry #%ld: %d", i, exec_entry_type);
			}
			
			Raven_rst_target_exec_array(id, RSTRING_PTR(exec_arg), RSTRING_LEN(exec_arg));
		}
		
		break;
	}
	case T_STRING:
	{
		Raven_rst_target_exec_string(id, RSTRING_PTR(kw_values[1]), RSTRING_LEN(kw_values[1]));
		break;
	}
	default:
	{
		rb_raise(rb_eTypeError, "invalid type of the exec argument: %d", exec_type);
		break;
	}
	}
	
	return result;
}

static VALUE Raven_function_targets(VALUE obj)
{
	(void) obj;
	return rb_ivar_get(static_raven_module, rb_intern("@targets"));
}

static VALUE Raven_function_env(int argc, VALUE *argv, VALUE obj)
{
	(void) obj;
	VALUE key;
	VALUE result = Qnil;
	int scan_result = rb_scan_args(argc, argv, "11", &key, &result);
	const char* key_str;
	const enum ruby_value_type key_type = rb_type(key);
	if (key_type == T_STRING)
	{
		key_str = RSTRING_PTR(key);
	}
	else
	{
		rb_raise(rb_eTypeError, "invalid type of the key argument: %d", key_type);
	}
	
	const char* value = getenv(key_str);
	
	if (value != NULL)
	{
		result = rb_funcall(static_environment_variable_class, rb_intern("new"), 1, rb_str_new_cstr(value));
	}
	else if (scan_result == 2)
	{
		result = rb_funcall(static_environment_variable_class, rb_intern("new"), 1, result);
	}
	else
	{
		rb_raise(rb_eKeyError, "environment variable '%s' is not defined", key_str);
	}
	
	return result;
}

static VALUE Raven_environment_variable_function_to_array(VALUE obj)
{
	VALUE result = Qundef;
	const char* err = NULL;
	const char* string = RSTRING_PTR(obj);
	
	if (! static_script_info->use_wordexp)
	{
		result = expand(string, &err);
		if (result == Qundef)
		{
			rb_raise(rb_eRuntimeError, "string '%s' could not be expanded: %s", string, err);
		}
	}
	else
	{
		wordexp_t wordexp_str;
		int w_result = wordexp(string, &wordexp_str, WRDE_NOCMD | WRDE_UNDEF);
		switch (w_result)
		{
			case 0:
			{
				break;
			}
			case WRDE_NOSPACE:
			{
				err = "wordexp: out of memory";
				break;
			}
			case WRDE_BADCHAR:
			{
				err = "wordexp: a metachar appears in the wrong place";
				break;
			}
			case WRDE_BADVAL:
			{
				err = "wordexp: undefined var reference with WRDE_UNDEF";
				break;
			}
			case WRDE_CMDSUB:
			{
				err = "wordexp: command substitution with WRDE_NOCMD";
				break;
			}
			case WRDE_SYNTAX:
			{
				err = "wordexp: shell syntax error";
				break;
			}
			default:
			{
				err = "wordexp returned invalid error code";
				break;
			}
		}
		
		if (err)
		{
			wordfree(&wordexp_str);
			rb_raise(rb_eRuntimeError, "wordexp with string '%s': %s", string, err);
		}
		
		result = rb_ary_new_capa((long) wordexp_str.we_wordc);
		for (size_t i = 0; i != wordexp_str.we_wordc; ++i)
		{
			rb_ary_push(result, rb_str_new_cstr(wordexp_str.we_wordv[i]));
		}
		wordfree(&wordexp_str);
	}
	
	return result;
}

static VALUE Raven_function_spaced_array_to_string(VALUE obj)
{
	VALUE result = rb_str_new("", 0);
	long len = rb_array_len(obj);
	for (long i = 0; i != len; ++i)
	{
		if (i)
		{
			rb_str_cat(result, " ", 1);
		}
		rb_str_concat(result, rb_funcall(rb_ary_entry(obj, i), rb_intern("to_s"), 0));
	}
	return result;
}

static VALUE Raven_function_target_to_string(VALUE obj)
{
	return Raven_function_spaced_array_to_string(rb_ivar_get(obj, rb_intern("@targets")));
}

extern void Raven_cxx_rst_get_header_dependencies(struct String_view source_file, VALUE result);

static VALUE Raven_cxx_get_header_dependencies(VALUE obj, VALUE source_file, VALUE result)
{
	(void) obj;
	struct String_view str = {.data = RSTRING_PTR(source_file), .len = (unsigned long) RSTRING_LEN(source_file)};
	Raven_cxx_rst_get_header_dependencies(str, result);
	return Qnil;
}


void Raven_raise(const char* message)
{
	rb_raise(rb_eRuntimeError, "%s", message);
}

int Raven_main(void)
{
	ruby_init();
	
	static_raven_module = rb_define_module(static_script_info->libname);
	static_environment_variable_class = rb_define_class_under(static_raven_module, "Environment_variable", rb_cString);
	static_target_class = rb_define_class_under(static_raven_module, "Target", rb_cObject);
	static_spaced_array_class = rb_define_class_under(static_raven_module, "Spaced_array", rb_cArray);
	
	VALUE raven_targets = rb_hash_new_capa((long) static_script_info->num_targets);
	rb_ivar_set(static_raven_module, rb_intern("@targets"), raven_targets);
	{
		size_t beg = 0;
		for (unsigned long i = 0; i != static_script_info->num_targets; ++i)
		{
			size_t len = 0;
			while (static_script_info->targets[beg + len] != '\0')
			{
				++len;
			}
			rb_hash_aset(raven_targets, ID2SYM(rb_intern2(static_script_info->targets + beg, (long) len)), Qtrue);
			beg += len + 1;
		}
	}
	rb_obj_freeze(rb_ivar_get(static_raven_module, rb_intern("@targets")));
	rb_define_const(static_raven_module, "TARGETS", raven_targets);
	rb_ivar_set(static_raven_module, rb_intern("@implicit_directories"), Qtrue);
	
	rb_define_module_function(static_raven_module, "shell", Raven_function_shell, -1);
	rb_define_module_function(static_raven_module, "implicit_directories", Raven_function_implicit_directories, 1);
	rb_define_module_function(static_raven_module, "targets", Raven_function_targets, 0);
	rb_define_module_function(static_raven_module, "env", Raven_function_env, -1);
	rb_define_module_function(static_raven_module, "target", Raven_function_target, -1);
	// rb_define_module_function(static_target_class, "initialize", Raven_function_target, -1);
	
	static_raven_cxx_module = rb_define_module_under(static_raven_module, "Cxx");
	rb_define_module_function(static_raven_cxx_module, "get_header_dependencies", Raven_cxx_get_header_dependencies, 2);
	static_raven_cxx_module = rb_define_module_under(static_raven_module, "Cxx");
	
	rb_define_method(static_environment_variable_class, "to_a", Raven_environment_variable_function_to_array, 0);
	rb_define_method(static_environment_variable_class, "to_ary", Raven_environment_variable_function_to_array, 0);
	
	rb_define_method(static_target_class, "to_a", Raven_function_target_to_array, 0);
	rb_define_method(static_target_class, "to_ary", Raven_function_target_to_array, 0);
	
	rb_define_method(static_target_class, "to_s", Raven_function_target_to_string, 0);
	
	rb_define_method(static_spaced_array_class, "to_s", Raven_function_spaced_array_to_string, 0);
	rb_define_method(static_spaced_array_class, "to_str", Raven_function_spaced_array_to_string, 0);
	
	// rb_define_const(rb_cObject, "ENV", rb_hash_new());
	
	// rb_cvar_set(static_raven_module, rb_intern("@@all_rules"), rb_ary_new_capa(32));
	{
		// VALUE values[] = {ID2SYM(rb_intern("s")), ID2SYM(rb_intern("t"))};
		// rb_funcallv(static_raven_rule_class, rb_intern("attr_reader"), sizeof(values) / sizeof(*values), values);
	}
	
	rb_funcallv(static_environment_variable_class, rb_intern("extend"), 1, (VALUE[]) {static_raven_module});
	rb_funcallv(static_raven_module, rb_intern("extend"), 1, (VALUE[]) {static_raven_module});
	
	// rb_funcallv(static_raven_module, rb_intern("private_constant"), 1, (VALUE[]) {ID2SYM(rb_intern("Environment_variable"))});
	
	VALUE script_file = rb_str_new(static_script_info->script_filename.data, (long) static_script_info->script_filename.len);
	int state = 0;
	fprintf(stderr, "raven: reading file %s\n", RSTRING_PTR(script_file));
	rb_load_protect(script_file, 0, &state);
	
	static_spaced_array_class = Qundef;
	static_target_class = Qundef;
	static_environment_variable_class = Qundef;
	static_raven_cxx_module = Qundef;
	static_raven_module = Qundef;
	
	ruby_cleanup(0);
	
	return state;
}

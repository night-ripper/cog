use std::io::Read;

fn main() -> std::process::ExitCode
{
	let mut exitcode = std::process::ExitCode::SUCCESS;
	
	let mut args = std::env::args_os();
	let raven_bin_name = args.next().unwrap().into_string().unwrap();
	let args = match raven::arguments::Arguments::new(args)
	{
		Ok(args) => {args}
		Err(error) =>
		{
			eprintln!("raven: {}", error);
			return 1.into();
		}
	};
	
	if args.help || std::env::args_os().skip(1).next().is_none()
	{
		print!("\
Usage: {} [options]... <targets>...
Options:
  -h, --help        Print help message.
  --clean           Clean all targets.

  -C <curdir>       Set the current directory to <curdir>
  -f <file>         Read the dependency graph from <file>
  -j <jobs>         Limit the number of concurrent jobs to <jobs>
", raven_bin_name);
		return exitcode;
	}
	
	if let Err(error) = std::env::set_current_dir(&args.current_directory)
	{
		eprintln!("raven: can not set current directory {:?}: {}", args.current_directory, error);
		return 1.into();
	}
	
	let table =
	{
		let mut filename = std::fs::File::open(args.filename.as_os_str()).unwrap();
		let mut string = String::with_capacity(128);
		filename.read_to_string(&mut string);
		match toml::from_str::<toml::Table>(string.as_str())
		{
			Ok(table) => table,
			Err(error) =>
			{
				eprint!("raven: {:?}: {}", args.filename, error);
				return 1.into();
			},
		}
	};
	
	raven::which::WHICH.set(raven::which::which_mapping()).unwrap();
	
	for entry in raven::which::WHICH.get().unwrap()
	{
		// println!("{:?} -> {:?}", entry.0, entry.1);
	}
	
	let Some(plugins) = table.get("plugin") else
	{
		return exitcode;
	};
	
	let plugins = match plugins
	{
		toml::Value::Table(table) => {table}
		value =>
		{
			eprintln!("raven: {:?}: unexpected value of \"plugin\": {:?}", args.filename, value);
			return 1.into();
		},
	};
	
	for entry in plugins
	{
		match unsafe {libloading::Library::new(format!("target/rust/x86_64-unknown-linux-gnu/debug/lib{}.so", entry.0.as_str()))}
		{
			Err(error) =>
			{
				eprintln!("raven: plugin {}: {}", entry.0.as_str(), error);
				return 1.into();
			}
			Ok(plugin) =>
			{
				let arguments = match entry.1
				{
					toml::Value::Table(table) => {table}
					value =>
					{
						todo!("{}", value);
					}
				};
				let new = match unsafe {plugin.get::<fn(&raven::plugin::Table) -> Box<dyn raven::plugin::Plugin>>(b"new")}
				{
					Err(error) => {todo!("no constructor found: {}", error)}
					Ok(new) => {new}
				};
				let mut plugin = new(arguments);
				plugin.run();
			}
		};
		println!("{}", entry.0);
	}
	
	println!("{:?}", table);
	
	return exitcode;
}

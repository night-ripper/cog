pub fn which_mapping() -> std::collections::BTreeMap::<std::ffi::OsString, std::ffi::OsString>
{
	let mut result = std::collections::BTreeMap::<std::ffi::OsString, std::ffi::OsString>::new();
	
	for path in std::env::var_os("PATH").unwrap_or_default().as_encoded_bytes().split(|&b| b == b':')
	{
		let path = unsafe {std::ffi::OsStr::from_encoded_bytes_unchecked(path)};
		if let Ok(read_dir) = std::fs::read_dir(path)
		{
			for dir_entry in read_dir
			{
				if let Ok(dir_entry) = dir_entry
				{
					let path = dir_entry.path();
					if let Some(file_name) = path.file_name()
					{
						match result.entry(file_name.to_os_string())
						{
							std::collections::btree_map::Entry::Vacant(entry) =>
							{
								entry.insert(path.into_os_string());
							}
							_ => {}
						}
					}
				}
			}
		}
	}
	
	return result;
}

pub static WHICH: std::sync::OnceLock<std::collections::BTreeMap::<std::ffi::OsString, std::ffi::OsString>> = std::sync::OnceLock::new();

pub fn which(executable: &std::ffi::OsStr) -> Option<&std::ffi::OsStr>
{
	WHICH.get().unwrap().get(executable).map(std::ffi::OsString::as_os_str)
}

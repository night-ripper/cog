use crate::lua::*;
use crate::*;

pub static COG_REGISTRY_DEBUG_OBJECT: u8 = 0;
pub static COG_REGISTRY_DECIDERS: u8 = 0;
pub static COG_REGISTRY_DEFERRED_RECIPE: u8 = 0;
pub static COG_REGISTRY_RULE_METATBLE: u8 = 0;
pub static COG_RULE_INDEX: u8 = 0;

pub struct Lua_context
{
	pub lua: Lua,
	pub lua_debug: lua_Debug,
	pub implicit_directories: bool,
	pub implicit_executables: bool,
	pub clean_files: String_vec,
	pub shell: String_vec,
}

impl Lua_context
{
	pub fn new(args: &Arguments, shared_data: &mut Rule_data) -> Result<Self, String>
	{
		let mut lua = Lua::new();
		let mut lua_debug = lua_Debug::default();
		
		unsafe
		{
			luaL_openlibs(lua.object.as_mut());
			
			{
				let lib = [
					luaL_Reg {name: b"rule\0".as_ptr().cast(), func: Some(Rule_function::wrapped)},
					luaL_Reg {name: b"expand\0".as_ptr().cast(), func: Some(Expand_function::wrapped)},
					
					luaL_Reg {name: b"c_prerequisites\0".as_ptr().cast(), func: Some(C_prerequisites::wrapped)},
					luaL_Reg {name: b"c_dependency_file_for\0".as_ptr().cast(), func: Some(c_dependency_file_for)},
					
					luaL_Reg {name: std::ptr::null(), func: None},
				];
				
				lua_createtable(lua.object.as_mut(), 0, lib.len() as std::ffi::c_int - 1);
				lua_pushlightuserdata(lua.object.as_mut(), (shared_data as *mut Rule_data).cast());
				luaL_setfuncs(lua.object.as_mut(), lib.as_ptr(), 1);
				
				lua_createtable(lua.object.as_mut(), 0, args.targets.len() as std::ffi::c_int);
				for target in args.targets.iter()
				{
					lua.pushlstring(target.as_bytes());
					lua_pushboolean(lua.object.as_mut(), true.into());
					lua_rawset(lua.object.as_mut(), -3);
				}
				lua_setfield(lua.object.as_mut(), -2, b"targets\0".as_ptr().cast());
				
				lua_createtable(lua.object.as_mut(), 0, 0);
				lua_setfield(lua.object.as_mut(), -2, b"clean\0".as_ptr().cast());
				
				lua_createtable(lua.object.as_mut(), 3, 0);
				for (i, &string) in [b"sh".as_slice(), b"-eux".as_slice(), b"-c".as_slice()].iter().enumerate()
				{
					lua.pushlstring(string);
					lua_rawseti(lua.object.as_mut(), -2, i as lua_Integer + 1);
				}
				lua_setfield(lua.object.as_mut(), -2, b"shell\0".as_ptr().cast());
				
				lua_pushboolean(lua.object.as_mut(), true.into());
				lua_setfield(lua.object.as_mut(), -2, b"implicit_directories\0".as_ptr().cast());
				
				lua_pushboolean(lua.object.as_mut(), true.into());
				lua_setfield(lua.object.as_mut(), -2, b"implicit_executables\0".as_ptr().cast());
				
				lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_REGISTRY_DEBUG_OBJECT).cast_mut().cast());
				lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of_mut!(lua_debug).cast());
				lua_rawset(lua.object.as_mut(), LUA_REGISTRYINDEX);
				
				lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_REGISTRY_DECIDERS).cast_mut().cast());
				lua_createtable(lua.object.as_mut(), 32, 0);
				lua_rawset(lua.object.as_mut(), LUA_REGISTRYINDEX);
				
				lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_REGISTRY_DEFERRED_RECIPE).cast_mut().cast());
				lua_createtable(lua.object.as_mut(), 32, 0);
				lua_rawset(lua.object.as_mut(), LUA_REGISTRYINDEX);
				
				let rule_funcs = [
					luaL_Reg {name: b"dep\0".as_ptr().cast(), func: Some(Dep_function::wrapped)},
					luaL_Reg {name: b"recipe\0".as_ptr().cast(), func: Some(Recipe_function::wrapped)},
					luaL_Reg {name: b"implicit_directories\0".as_ptr().cast(), func: Some(Implicit_directories_function::wrapped)},
					luaL_Reg {name: std::ptr::null(), func: None},
				];
				
				lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_REGISTRY_RULE_METATBLE).cast_mut().cast());
				lua_createtable(lua.object.as_mut(), 0, 0);
				lua_pushnil(lua.object.as_mut());
				lua_copy(lua.object.as_mut(), -2, -1);
				lua_setfield(lua.object.as_mut(), -2, b"__index\0".as_ptr().cast());
				lua_pushlightuserdata(lua.object.as_mut(), (shared_data as *mut Rule_data).cast());
				luaL_setfuncs(lua.object.as_mut(), rule_funcs.as_ptr(), 1);
				lua_rawset(lua.object.as_mut(), LUA_REGISTRYINDEX);
				
				lua_setglobal(lua.object.as_mut(), args.libname.as_ptr());
			}
			
			let mut luaname = Vec::with_capacity(args.filename.len() + 1);
			luaname.extend_from_slice(args.filename.to_str().unwrap().as_bytes());
			luaname.push(b'\0');
			
			let load_result = luaL_loadfilex(lua.object.as_mut(), luaname.as_ptr().cast(), std::ptr::null());
			
			if load_result != 0
			{
				if lua_gettop(lua.object.as_mut()) > 0 && lua_isstring(lua.object.as_mut(), -1) != 0
				{
					return Err(format!("{}", std::str::from_utf8_unchecked(lua.tolstring(-1))));
				}
				else
				{
					return Err(format!("when opening file {:?}: {}", args.filename, load_result));
				}
			}
			else
			{
				match lua_pcall(lua.object.as_mut(), 0, -1, 0)
				{
					0 => {}
					error =>
					{
						return Err(format!("({}) error: when reading file {:?}:{}: {}",
							error, args.filename, lua_debug.currentline, std::str::from_utf8_unchecked(lua.tolstring(-1))
						));
					}
				}
			}
			
			let implicit_directories;
			let implicit_executables;
			let mut clean_files;
			let mut shell;
			
			lua_getglobal(lua.object.as_mut(), args.libname.as_ptr());
			////
			lua_getfield(lua.object.as_mut(), -1, b"implicit_directories\0".as_ptr().cast());
			implicit_directories = lua_toboolean(lua.object.as_mut(), -1) != 0;
			lua_pop(lua.object.as_mut(), 1);
			////
			lua_getfield(lua.object.as_mut(), -1, b"implicit_executables\0".as_ptr().cast());
			implicit_executables = lua_toboolean(lua.object.as_mut(), -1) != 0;
			lua_pop(lua.object.as_mut(), 1);
			////
			lua_getfield(lua.object.as_mut(), -1, b"clean\0".as_ptr().cast());
			let len = lua_rawlen(lua.object.as_mut(), -1) as usize;
			clean_files = String_vec::default();
			
			for i in 0 .. len as usize
			{
				if lua_rawgeti(lua.object.as_mut(), -1, i as lua_Integer + 1) != LUA_TSTRING
				{
					return Err(format!("clean table contains a non-string"));
				}
				clean_files.push(lua.tolstring(-1));
				lua_pop(lua.object.as_mut(), 1);
			}
			lua_pop(lua.object.as_mut(), 1);
			////
			lua_getfield(lua.object.as_mut(), -1, b"shell\0".as_ptr().cast());
			let len = lua_rawlen(lua.object.as_mut(), -1) as usize;
			if len == 0
			{
				return Err(format!("shell table is empty"));
			}
			shell = String_vec::default();
			
			for i in 0 .. len as usize
			{
				if lua_rawgeti(lua.object.as_mut(), -1, i as lua_Integer + 1) != LUA_TSTRING
				{
					return Err(format!("shell table contains a non-string"));
				}
				shell.push(lua.tolstring(-1));
				lua_pop(lua.object.as_mut(), 1);
			}
			lua_pop(lua.object.as_mut(), 1);
			////
			lua_pop(lua.object.as_mut(), 1);
			
			return Ok(Self {lua, lua_debug, implicit_directories, implicit_executables, clean_files, shell});
		}
	}
}

impl Rule
{
	pub fn needs_remake(&self, entry_index: usize, context: &Lua_context) -> Result<bool, String>
	{
		let mut lua = context.lua.clone();
		let lua_debug = &context.lua_debug;
		
		unsafe
		{
			// execute deciders
			lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_REGISTRY_DECIDERS).cast_mut().cast());
			lua_rawget(lua.object.as_mut(), LUA_REGISTRYINDEX);
			lua_rawgeti(lua.object.as_mut(), -1, entry_index as lua_Integer);
			
			let mut result = false;
			
			let len = lua_rawlen(lua.object.as_mut(), -1);
			
			if len > 0
			{
				lua_createtable(lua.object.as_mut(),
					self.targets.len() as std::ffi::c_int, 0
				);
				for (i, target) in self.targets.iter().enumerate()
				{
					lua.pushlstring(target.as_bytes());
					lua_rawseti(lua.object.as_mut(), -2, i as lua_Integer + 1);
				}
				for i in 0 .. len
				{
					assert_eq!(LUA_TFUNCTION, lua_rawgeti(lua.object.as_mut(), -2, i as lua_Integer + 1));
					lua_pushnil(lua.object.as_mut());
					lua_copy(lua.object.as_mut(), -3, -1);
					if lua_pcall(lua.object.as_mut(), 1, 1, 0) != 0
					{
						return Err(format!("{}:{}", lua_debug.currentline, std::str::from_utf8_unchecked(lua.tolstring(-1))));
					}
					
					result |= match lua_type(lua.object.as_mut(), -1)
					{
						LUA_TNIL => {false}
						LUA_TBOOLEAN => {lua_toboolean(lua.object.as_mut(), -1) != 0}
						typeid =>
						{
							return Err(format!("decider returned type: {}", typeid));
						}
					};
					lua_pop(lua.object.as_mut(), 1);
					
					if result == true
					{
						break;
					}
				}
			}
			lua_pop(lua.object.as_mut(), 2);
			
			Ok(result)
		}
	}
	
	pub fn infer_recipe(&mut self, entry_index: usize, context: &Lua_context) -> Result<(), String>
	{
		let mut lua = context.lua.clone();
		let lua_debug = &context.lua_debug;
		
		match &self.recipe
		{
			Some(Recipe::deferred) => unsafe
			{
				lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_REGISTRY_DEFERRED_RECIPE).cast_mut().cast());
				lua_rawget(lua.object.as_mut(), LUA_REGISTRYINDEX);
				lua_rawgeti(lua.object.as_mut(), -1, entry_index as lua_Integer);
				
				lua_createtable(lua.object.as_mut(), self.targets.len() as std::ffi::c_int, 0);
				for (i, target) in self.targets.iter().enumerate()
				{
					lua.pushlstring(target.as_bytes());
					lua_rawseti(lua.object.as_mut(), -2, i as lua_Integer + 1);
				}
				
				lua_createtable(lua.object.as_mut(), self.prerequisites.len() as std::ffi::c_int, 0);
				for (i, prerequisite) in self.prerequisites.iter().enumerate()
				{
					lua.pushlstring(prerequisite.as_bytes());
					lua_rawseti(lua.object.as_mut(), -2, i as lua_Integer + 1);
				}
				
				if lua_pcall(lua.object.as_mut(), 2, 1, 0) != 0
				{
					return Err(format!("{}: {}", lua_debug.currentline, std::str::from_utf8_unchecked(lua.tolstring(-1))));
				}
				
				self.recipe = match lua_type(lua.object.as_mut(), -1)
				{
					LUA_TNIL => {None}
					LUA_TSTRING =>
					{
						Some(Recipe::from_script(lua))
					}
					LUA_TTABLE => match Recipe::from_command(lua)
					{
						Ok(recipe) => {Some(recipe)}
						Err(error) => {return Err(error)}
					}
					typeid => {return Err(format!("recipe type: {}", typeid))}
				};
				lua_pop(lua.object.as_mut(), 1);
			}
			_ => {}
		}
		
		Ok(())
	}
}

#[derive(Default)]
pub struct Lua_function
{
	data: Vec<u8>,
}

impl std::fmt::Debug for Lua_function
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		for &b in &self.data
		{
			write!(f, "{:X}", b)?;
		}
		
		Ok(())
	}
}

impl Lua_function
{
	unsafe fn dump(lua: *mut std::ffi::c_void) -> Self
	{
		let mut data = Vec::<u8>::with_capacity(64);
		lua_dump(lua, Self::writer_func, std::ptr::addr_of_mut!(data).cast(), 0);
		Self {data}
	}
	
	unsafe fn load(&self, lua: *mut std::ffi::c_void, chunkname: *const std::ffi::c_char)
	{
		match lua_load(lua, Self::reader_func, (&self.data as *const Vec<u8> as *mut Vec<u8>).cast(), chunkname, b"b\0".as_ptr().cast())
		{
			LUA_OK => (),
			LUA_ERRSYNTAX => panic!("lua_load failed: syntax error during precompilation"),
			LUA_ERRMEM => panic!("lua_load failed: memory allocation (out-of-memory) error"),
			code => unreachable!("lua_load returned code: {}", code),
		}
	}
	
	unsafe extern "C" fn writer_func(_lua: *mut std::ffi::c_void, data: *const std::ffi::c_void,
		size: usize, user_data: *mut std::ffi::c_void) -> std::ffi::c_int
	{
		let user_data = user_data.cast::<Vec<u8>>().as_mut().unwrap();
		user_data.extend_from_slice(std::slice::from_raw_parts(data.cast(), size));
		return 0;
	}
	
	unsafe extern "C" fn reader_func(_lua: *mut std::ffi::c_void, user_data: *mut std::ffi::c_void, size: *mut usize) -> *const std::ffi::c_char
	{
		let user_data = user_data.cast::<Vec<u8>>().as_ref().unwrap();
		*size = user_data.len();
		return user_data.as_ptr().cast();
	}
}

pub trait Wrapped_lua_function
{
	unsafe fn function(lua: Lua_state) -> Result<std::ffi::c_int, String>;
	
	unsafe extern "C" fn wrapped(mut lua: Lua_state) -> std::ffi::c_int
	{
		match Self::function(lua)
		{
			Ok(result) => {return result;}
			Err(error) =>
			{
				lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_REGISTRY_DEBUG_OBJECT).cast_mut().cast());
				lua_rawget(lua.object.as_mut(), LUA_REGISTRYINDEX);
				let lua_debug = lua_touserdata(lua.object.as_mut(), -1).cast::<lua_Debug>();
				lua_getstack(lua.object.as_mut(), 1, &mut *lua_debug);
				lua_getinfo(lua.object.as_mut(), b"l\0".as_ptr().cast(), &mut *lua_debug);
				lua.pushlstring(error.as_bytes());
			}
		}
		
		lua_error(lua.object.as_mut());
	}
}

pub unsafe extern "C" fn c_dependency_file_for(mut lua: Lua_state) -> std::ffi::c_int
{
	lua.pushlstring(std::str::from_utf8_unchecked(
		crate::c_dependency_file_for_core(lua.clone().tolstring(-1)).as_slice()
	).as_bytes());
	1
}

pub struct Rule_function {}
impl Wrapped_lua_function for Rule_function
{
	unsafe fn function(mut lua: Lua_state) -> Result<std::ffi::c_int, String>
	{
		let shared_data = lua_touserdata(lua.object.as_mut(), lua_upvalueindex(1)).cast::<Rule_data>().as_mut().unwrap();
		let mut rule_index = usize::MAX;
		let stack_top = lua_gettop(lua.object.as_mut());
		lua_createtable(lua.object.as_mut(), stack_top, 1);
		
		for idx in (0 .. stack_top).rev()
		{
			match lua_type(lua.object.as_mut(), -idx - 2)
			{
				LUA_TSTRING =>
				{
					lua_pushnil(lua.object.as_mut());
					lua_copy(lua.object.as_mut(), -idx - 3, -1);
					lua_rawseti(lua.object.as_mut(), -2, stack_top as lua_Integer - idx as lua_Integer);
					let target = std::str::from_utf8_unchecked(lua.tolstring(-idx - 2));
					
					match shared_data.targets.entry(target.to_string())
					{
						std::collections::btree_map::Entry::Vacant(entry) =>
						{
							if rule_index == usize::MAX
							{
								rule_index = shared_data.rules.len();
								shared_data.rules.push(Rule::default());
							}
							entry.insert(Target_info {rule_index, mtime: None});
						}
						std::collections::btree_map::Entry::Occupied(entry) =>
						{
							if rule_index == usize::MAX
							{
								rule_index = entry.get().rule_index;
							}
							if rule_index != entry.get().rule_index
							{
								return Err(format!("rule: multiple rules to make target {:?}", entry.key()));
							}
						}
					}
					
					shared_data.rules[rule_index].targets.push(target.as_bytes());
				}
				typeid => {return Err(format!("rule: typeid: {}", typeid))}
			}
		}
		
		// a rule with no target is still valid
		if rule_index == usize::MAX
		{
			rule_index = shared_data.rules.len();
			shared_data.rules.push(Rule::default());
		}
		
		lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_RULE_INDEX).cast_mut().cast());
		lua_pushinteger(lua.object.as_mut(), rule_index as lua_Integer);
		lua_rawset(lua.object.as_mut(), -3);
		
		lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_REGISTRY_RULE_METATBLE).cast_mut().cast());
		lua_rawget(lua.object.as_mut(), LUA_REGISTRYINDEX);
		lua_setmetatable(lua.object.as_mut(), -2);
		
		Ok(1)
	}
}

pub struct Dep_function {}
impl Wrapped_lua_function for Dep_function
{
	unsafe fn function(mut lua: Lua_state) -> Result<std::ffi::c_int, String>
	{
		let stack_top = lua_gettop(lua.object.as_mut());
		
		if stack_top < 2
		{
			return Err(format!("dep: expected at least 2 arguments"));
		}
		let shared_data = lua_touserdata(lua.object.as_mut(), lua_upvalueindex(1)).cast::<Rule_data>().as_mut().unwrap();
		lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_RULE_INDEX).cast_mut().cast());
		lua_rawget(lua.object.as_mut(), -stack_top - 1);
		let rule_index = lua_tointeger(lua.object.as_mut(), -1) as usize;
		
		for idx in (0 .. stack_top - 1).rev()
		{
			match lua_type(lua.object.as_mut(), -idx - 2)
			{
				LUA_TSTRING =>
				{
					shared_data.rules[rule_index].prerequisites.push(lua.tolstring(-idx - 2));
				}
				LUA_TFUNCTION =>
				{
					lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_REGISTRY_DECIDERS).cast_mut().cast());
					lua_rawget(lua.object.as_mut(), LUA_REGISTRYINDEX);
					if lua_rawgeti(lua.object.as_mut(), -1, rule_index as lua_Integer) == LUA_TNIL
					{
						lua_createtable(lua.object.as_mut(), 1, 0);
						lua_copy(lua.object.as_mut(), -1, -2);
						lua_rawseti(lua.object.as_mut(), -3, rule_index as lua_Integer);
					}
					lua_pushnil(lua.object.as_mut());
					lua_copy(lua.object.as_mut(), -5, -1);
					lua_rawseti(lua.object.as_mut(), -2, lua_rawlen(lua.object.as_mut(), -2) as lua_Integer + 1);
					lua_pop(lua.object.as_mut(), 2);
				}
				LUA_TTABLE =>
				{
					lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_RULE_INDEX).cast_mut().cast());
					
					if lua_rawget(lua.object.as_mut(), -idx - 2 - 1) != LUA_TNUMBER
					{
						return Err(format!("dep: expected a rule table, missing rule index"));
					}
					let prerequisite_rule_index = lua_tointeger(lua.object.as_mut(), -1) as usize;
					let prerequisite_targets = std::mem::take(&mut shared_data.rules[prerequisite_rule_index].targets);
					shared_data.rules[rule_index].prerequisites.extend(&prerequisite_targets);
					shared_data.rules[prerequisite_rule_index].targets = prerequisite_targets;
					lua_pop(lua.object.as_mut(), 1);
				}
				typeid => {return Err(format!("dep: typeid: {}", typeid))}
			}
		}
		
		lua_pop(lua.object.as_mut(), stack_top - 1 + 1);
		
		Ok(1)
	}
}

pub struct Implicit_directories_function {}
impl Wrapped_lua_function for Implicit_directories_function
{
	unsafe fn function(mut lua: Lua_state) -> Result<std::ffi::c_int, String>
	{
		if lua_gettop(lua.object.as_mut()) != 2
		{
			return Err(format!("implicit_directories: expected exactly 2 arguments"))
		}
		if lua_type(lua.object.as_mut(), -1) != LUA_TBOOLEAN
		{
			return Err(format!("implicit_directories: expected a bool argument"));
		}
		lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_RULE_INDEX).cast_mut().cast());
		
		if lua_rawget(lua.object.as_mut(), -3) != LUA_TNUMBER
		{
			return Err(format!("implicit_directories: expected a rule table, missing rule index"));
		}
		let shared_data = lua_touserdata(lua.object.as_mut(), lua_upvalueindex(1)).cast::<Rule_data>().as_mut().unwrap();
		let rule_index = lua_tointeger(lua.object.as_mut(), -1) as usize;
		shared_data.rules[rule_index].implicit_directories = Some(lua_toboolean(lua.object.as_mut(), -2) != 0);
		lua_pop(lua.object.as_mut(), 2);
		
		Ok(1)
	}
}

pub struct Recipe_function {}
impl Wrapped_lua_function for Recipe_function
{
	unsafe fn function(mut lua: Lua_state) -> Result<std::ffi::c_int, String>
	{
		let stack_top = lua_gettop(lua.object.as_mut());
		
		if stack_top != 2
		{
			return Err(format!("recipe: expected exactly 2 arguments"));
		}
		
		let shared_data = lua_touserdata(lua.object.as_mut(), lua_upvalueindex(1)).cast::<Rule_data>().as_mut().unwrap();
		lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_RULE_INDEX).cast_mut().cast());
		lua_rawget(lua.object.as_mut(), -stack_top - 1);
		let rule_index = lua_tointeger(lua.object.as_mut(), -1) as usize;
		lua_pop(lua.object.as_mut(), 1);
		
		let recipe = match lua_type(lua.object.as_mut(), -1)
		{
			LUA_TSTRING => {Recipe::from_script(lua)}
			LUA_TFUNCTION =>
			{
				lua_pushlightuserdata(lua.object.as_mut(), std::ptr::addr_of!(COG_REGISTRY_DEFERRED_RECIPE).cast_mut().cast());
				lua_rawget(lua.object.as_mut(), LUA_REGISTRYINDEX);
				lua_rotate(lua.object.as_mut(), -2, 1);
				lua_rawseti(lua.object.as_mut(), -2, rule_index as lua_Integer);
				lua_pop(lua.object.as_mut(), 1);
				Recipe::deferred
			}
			LUA_TTABLE => match Recipe::from_command(lua)
			{
				Ok(recipe) => {recipe}
				Err(error) => {return Err(format!("recipe: {}", error))}
			}
			typeid => {return Err(format!("recipe: typeid: {}", typeid))}
		};
		
		if shared_data.rules[rule_index].recipe.replace(recipe).is_some()
		{
			return Err(format!("overriding recipe for {:?}", shared_data.rules[rule_index].targets))
		}
		
		Ok(1)
	}
}

pub struct C_prerequisites {}
impl Wrapped_lua_function for C_prerequisites
{
	unsafe fn function(mut lua: Lua_state) -> Result<std::ffi::c_int, String>
	{
		let shared_data = lua_touserdata(lua.object.as_mut(), lua_upvalueindex(1)).cast::<crate::Rule_data>().as_mut().unwrap();
		
		lua_rawgeti(lua.object.as_mut(), -1, 1);
		let target_file_name = std::str::from_utf8_unchecked(lua.tolstring(-1));
		let target_path = std::path::Path::new(target_file_name);
		let dependency_file = target_path.parent().unwrap().parent().unwrap().join("dependencies")
			.join(target_path.file_stem().unwrap()).with_extension("mk")
		;
		let mut stream = match std::fs::File::open(dependency_file)
		{
			Ok(result) => {result}
			Err(error) if error.kind() == std::io::ErrorKind::NotFound => {return Ok(0)}
			Err(error) => {return Err(format!("{}", error))}
		};
		
		let prerequisites = match crate::read_make(&mut stream)
		{
			Ok(result) => {result}
			Err(error) => {return Err(format!("{}", error))}
		};
		
		for (target, prerequisites) in prerequisites.iter()
		{
			if target == target_file_name
			{
				let target_mtime = match shared_data.get_mtime(target_file_name)
				{
					Err(error) => {return Err(format!("error: {:?}", error))}
					Ok(mtime) => {mtime}
				};
				
				for prerequisite in prerequisites.iter()
				{
					let prerequisite_mtime = match shared_data.get_mtime(prerequisite)
					{
						Err(error) => {return Err(format!("error: {:?}", error))}
						Ok(mtime) => {mtime}
					};
					
					if target_mtime < prerequisite_mtime
					{
						lua_pushboolean(lua.object.as_mut(), true.into());
						return Ok(1);
					}
				}
			}
		}
		
		lua_pushboolean(lua.object.as_mut(), false.into());
		return Ok(1);
	}
}


pub struct Expand_function {}
impl Wrapped_lua_function for Expand_function
{
	unsafe fn function(mut lua: Lua_state) -> Result<std::ffi::c_int, String>
	{
		// TODO handle nil
		
		let mut result = crate::String_vec::default();
		crate::expand(lua.tolstring(-1), &mut result)?;
		lua_createtable(lua.object.as_mut(), result.len() as std::ffi::c_int, 0);
		for (i, string) in result.iter().enumerate()
		{
			lua.pushlstring(string.as_bytes());
			lua_rawseti(lua.object.as_mut(), -2, i as lua_Integer + 1);
		}
		
		Ok(1)
	}
}

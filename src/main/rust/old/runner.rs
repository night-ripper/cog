pub enum Task_result
{
	success,
	stalled,
	error,
}

pub struct Task
{
	num_dependencies: usize,
	dependants: Vec<usize>,
	function: Option<Box<dyn std::ops::FnMut(&mut Runner) -> Task_result + Send>>,
}

pub struct Runner
{
	tasks: crate::repository::Repository<Task>,
	tasks_ready: std::collections::VecDeque<usize>,
}

impl Runner
{
	pub fn insert(&mut self, function: impl std::ops::FnMut(&mut Runner) -> Task_result + 'static + Send, dependants: &[usize]) -> usize
	{
		let task = Task
		{
			num_dependencies: 0,
			dependants: Vec::from(dependants),
			function: Some(Box::new(function)),
		};
		for &dependant_index in dependants
		{
			self.tasks[dependant_index].num_dependencies += 1;
		}
		let result = self.tasks.insert(task);
		self.tasks_ready.retain(|&index|
		{
			self.tasks[index].num_dependencies == 0
		});
		self.tasks_ready.push_back(result);
		return result;
	}
	
	pub fn run(&mut self) -> Result<(), String>
	{
		let mut result = Ok(());
		struct Context
		{
			end: bool,
			threads_remaining: usize,
		}
		
		let condvar = std::sync::Condvar::new();
		let context = std::sync::Mutex::new(Context {end: false, threads_remaining: 0});
		
		std::thread::scope(|scope|
		{
			let context = &context;
			let condvar = &condvar;
			
			let mut threads = Vec::new();
			threads.reserve(std::thread::available_parallelism().unwrap_or(1.try_into().unwrap()).get());
			let capacity = threads.capacity();
			
			for _ in 0 .. capacity
			{
				threads.push(scope.spawn(move || -> Result<(), String>
				{
					loop
					{
						let mut guard = context.lock().unwrap();
						loop
						{
							if guard.end
							{
								return Ok(());
							}
							
							match None::<usize>
							{
								Some(_index) =>
								{
									// TODO
									break;
								}
								None =>
								{
									guard.threads_remaining += 1;
									if guard.threads_remaining == capacity
									{
										guard.end = true;
										condvar.notify_all();
										return Ok(());
									}
									guard = condvar.wait(guard).unwrap();
									if guard.end
									{
										return Ok(());
									}
									guard.threads_remaining -= 1;
								}
							}
						}
						drop(guard);
					}
				}));
			}
			
			for thread in threads.into_iter()
			{
				if let Err(error) = thread.join().unwrap()
				{
					result = Err(error);
				}
			}
		});
		return result;
	}
}

impl Default for Runner
{
	fn default() -> Self
	{
		Self
		{
			tasks: crate::repository::Repository::with_capacity(32),
			tasks_ready: std::collections::VecDeque::with_capacity(32),
		}
	}
}

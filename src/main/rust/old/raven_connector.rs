#[repr(C)]
pub struct String_view
{
	pub data: *const std::ffi::c_char,
	pub len: std::ffi::c_ulong,
}

impl From<&[u8]> for String_view
{
	fn from(value: &[u8]) -> Self
	{
		String_view {data: value.as_ptr().cast(), len: value.len() as std::ffi::c_ulong}
	}
}

impl AsRef<[u8]> for String_view
{
	fn as_ref(&self) -> &[u8]
	{
		unsafe {std::slice::from_raw_parts(self.data.cast(), self.len as usize)}
	}
}

#[link(name = "Raven", kind = "static")]
#[link(name = "ruby")]
extern "C"
{
	pub static mut static_script_info: *mut Script_info;
	
	pub fn Raven_raise(message: *const std::ffi::c_char) -> !;
	pub fn Raven_main() -> std::ffi::c_int;
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_shell(string: *const std::ffi::c_char, len: std::ffi::c_long)
{
	let string = std::slice::from_raw_parts(string as *const u8, len as usize);
	let command = static_script_info.as_mut().unwrap().shell_param.cast::<crate::String_vec>().as_mut().unwrap();
	command.push(string);
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_new_target(object_id: usize, implicit_directories: std::ffi::c_char) -> usize
{
	let script_info = static_script_info.as_mut().unwrap();
	let rules = script_info.rule_data.cast::<Vec<crate::Rule>>().as_mut().unwrap();
	let result = rules.len();
	let mut target = crate::Rule::default();
	if implicit_directories != -1
	{
		target.implicit_directories = Some(implicit_directories == 1);
	}
	rules.push(target);
	script_info.rule_ids.cast::<std::collections::HashMap<usize, u32>>().as_mut().unwrap().insert(object_id, result as u32);
	return result;
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_target_target_string(id: usize, name: *const std::ffi::c_char, len: std::ffi::c_long)
{
	let script_info = static_script_info.as_mut().unwrap();
	let string_index = script_info.string_repository.cast::<crate::Os_str_repository>().as_mut().unwrap().insert(
		crate::Os_str_repository::join_strings(b".", std::slice::from_raw_parts(name.cast(), len as usize)), id
	);
	let rules = script_info.rule_data.cast::<Vec<crate::Rule>>().as_mut().unwrap();
	rules[id].targets.push(string_index as u32);
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_target_target_symbol(id: usize, name: *const std::ffi::c_char)
{
	let script_info = static_script_info.as_mut().unwrap();
	let string_index = script_info.string_repository.cast::<crate::Os_str_repository>().as_mut().unwrap().insert(
		crate::Os_str_repository::join_strings(b":", std::ffi::CStr::from_ptr(name).to_bytes()), id
	);
	let rules = script_info.rule_data.cast::<Vec<crate::Rule>>().as_mut().unwrap();
	rules[id].targets.push(string_index as u32);
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_target_source_string(id: usize, name: *const std::ffi::c_char, len: std::ffi::c_long)
{
	let script_info = static_script_info.as_mut().unwrap();
	let string_index = script_info.string_repository.cast::<crate::Os_str_repository>().as_mut().unwrap().insert(
		crate::Os_str_repository::join_strings(b".", std::slice::from_raw_parts(name.cast(), len as usize)), usize::MAX
	);
	let rules = script_info.rule_data.cast::<Vec<crate::Rule>>().as_mut().unwrap();
	rules[id].sources.push(string_index as u32);
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_target_source_symbol(id: usize, name: *const std::ffi::c_char)
{
	let script_info = static_script_info.as_mut().unwrap();
	let string_index = script_info.string_repository.cast::<crate::Os_str_repository>().as_mut().unwrap().insert(
		crate::Os_str_repository::join_strings(b":", std::ffi::CStr::from_ptr(name).to_bytes()), usize::MAX
	);
	let rules = &mut script_info.rule_data.cast::<Vec<crate::Rule>>().as_mut().unwrap();
	rules[id].sources.push(string_index as u32);
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_target_source_target(id: usize, source_object_id: usize)
{
	let script_info = static_script_info.as_mut().unwrap();
	let rules = &mut script_info.rule_data.cast::<Vec<crate::Rule>>().as_mut().unwrap();
	let rule_ids = script_info.rule_ids.cast::<std::collections::HashMap<usize, usize>>().as_ref().unwrap();
	let mut sources = std::mem::take(&mut rules[id].sources);
	for &target in rules[*rule_ids.get(&source_object_id).unwrap()].targets.as_slice()
	{
		sources.push(target);
	}
	rules[id].sources = sources;
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_target_env(id: usize, key: *const std::ffi::c_char,
	key_len: std::ffi::c_long, value: *const std::ffi::c_char, value_len: std::ffi::c_long)
{
	let rules = static_script_info.as_mut().unwrap().rule_data.cast::<Vec<crate::Rule>>().as_mut().unwrap();
	rules[id].env.insert(
		std::ffi::OsStr::from_encoded_bytes_unchecked(std::slice::from_raw_parts(key.cast(), key_len as usize)).into(),
		std::ffi::OsStr::from_encoded_bytes_unchecked(std::slice::from_raw_parts(value.cast(), value_len as usize)).into(),
	);
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_target_exec_array(id: usize, arg: *const std::ffi::c_char, arg_len: std::ffi::c_long)
{
	let script_info = static_script_info.as_mut().unwrap();
	let rules = script_info.rule_data.cast::<Vec<crate::Rule>>().as_mut().unwrap();
	match rules[id].recipe.as_mut()
	{
		None =>
		{
			let mut executable = std::slice::from_raw_parts(arg.cast(), arg_len as usize).to_vec();
			let path = std::path::Path::new(std::ffi::OsStr::from_encoded_bytes_unchecked(&executable));
			if path.is_relative() && path.components().next() != Some(std::path::Component::CurDir)
			{
				executable.insert(0, b'e');
			}
			else
			{
				executable.insert(0, b'.');
			}
			let command = script_info.string_repository.cast::<crate::Os_str_repository>().as_mut().unwrap().insert(
				std::ffi::OsString::from_encoded_bytes_unchecked(executable), usize::MAX
			);
			rules[id].recipe.insert(crate::Recipe::command {command, args: Default::default()});
		}
		Some(crate::Recipe::command {command: _, args}) =>
		{
			args.push(std::slice::from_raw_parts(arg.cast(), arg_len as usize));
		}
		Some(crate::Recipe::script {script: _script}) =>
		{
			todo!();
		}
	};
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_target_exec_string(id: usize, string: *const std::ffi::c_char, len: std::ffi::c_long)
{
	let script_info = static_script_info.as_mut().unwrap();
	let rules = static_script_info.as_mut().unwrap().rule_data.cast::<Vec<crate::Rule>>().as_mut().unwrap();
	match rules[id].recipe.as_mut()
	{
		None =>
		{
			let slice = std::slice::from_raw_parts(string.cast(), len as usize);
			let mut expander = crate::Expander::from(&slice);
			let Some(Ok(mut executable)) = expander.next() else
			{
				Raven_raise(std::ffi::CStr::from_bytes_with_nul(b"failed to read command from the exec field\0").unwrap().as_ptr());
			};
			let path = std::path::Path::new(std::ffi::OsStr::from_encoded_bytes_unchecked(&executable));
			if path.is_relative() && path.components().next() != Some(std::path::Component::CurDir)
			{
				executable.insert(0, b'e');
			}
			else
			{
				executable.insert(0, b'.');
			}
			let command = script_info.string_repository.cast::<crate::Os_str_repository>().as_mut().unwrap().insert(
				std::ffi::OsString::from_encoded_bytes_unchecked(executable), usize::MAX
			);
			let mut args = crate::String_vec::default();
			for arg in expander
			{
				args.push(std::ffi::OsString::from_encoded_bytes_unchecked(arg.unwrap()).as_encoded_bytes());
			}
			rules[id].recipe.insert(crate::Recipe::command {command, args});
		}
		Some(crate::Recipe::command {command: _, args: _}) =>
		{
			todo!();
		}
		Some(crate::Recipe::script {script: _script}) =>
		{
			todo!();
		}
	};
}

#[repr(C)]
pub struct Script_info
{
	pub libname: *const std::ffi::c_char,
	pub script_filename: String_view,
	pub targets: *const std::ffi::c_char,
	pub shell_param: *mut std::ffi::c_void,
	pub rule_data: *mut std::ffi::c_void,
	pub rule_ids: *mut std::ffi::c_void,
	pub string_repository: *mut std::ffi::c_void,
	pub num_targets: std::ffi::c_ulong,
	pub implicit_directories: std::ffi::c_uchar,
	pub use_wordexp: std::ffi::c_uchar,
}

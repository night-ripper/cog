use std::ffi::*;

pub type VALUE = usize;

#[repr(C)]
pub enum ruby_value_type
{
	RUBY_T_NONE     = 0x00, /*< Non-object (swept etc.) */
	RUBY_T_OBJECT   = 0x01, /*< @see struct ::RObject */
	RUBY_T_CLASS    = 0x02, /*< @see struct ::RClass and ::rb_cClass */
	RUBY_T_MODULE   = 0x03, /*< @see struct ::RClass and ::rb_cModule */
	RUBY_T_FLOAT    = 0x04, /*< @see struct ::RFloat */
	RUBY_T_STRING   = 0x05, /*< @see struct ::RString */
	RUBY_T_REGEXP   = 0x06, /*< @see struct ::RRegexp */
	RUBY_T_ARRAY    = 0x07, /*< @see struct ::RArray */
	RUBY_T_HASH     = 0x08, /*< @see struct ::RHash */
	RUBY_T_STRUCT   = 0x09, /*< @see struct ::RStruct */
	RUBY_T_BIGNUM   = 0x0a, /*< @see struct ::RBignum */
	RUBY_T_FILE     = 0x0b, /*< @see struct ::RFile */
	RUBY_T_DATA     = 0x0c, /*< @see struct ::RTypedData */
	RUBY_T_MATCH    = 0x0d, /*< @see struct ::RMatch */
	RUBY_T_COMPLEX  = 0x0e, /*< @see struct ::RComplex */
	RUBY_T_RATIONAL = 0x0f, /*< @see struct ::RRational */
	
	RUBY_T_NIL      = 0x11, /*< @see ::RUBY_Qnil */
	RUBY_T_TRUE     = 0x12, /*< @see ::RUBY_Qfalse */
	RUBY_T_FALSE    = 0x13, /*< @see ::RUBY_Qtrue */
	RUBY_T_SYMBOL   = 0x14, /*< @see struct ::RSymbol */
	RUBY_T_FIXNUM   = 0x15, /*< Integers formerly known as Fixnums. */
	RUBY_T_UNDEF    = 0x16, /*< @see ::RUBY_Qundef */
	
	RUBY_T_IMEMO    = 0x1a, /*< @see struct ::RIMemo */
	RUBY_T_NODE     = 0x1b, /*< @see struct ::RNode */
	RUBY_T_ICLASS   = 0x1c, /*< Hidden classes known as IClasses. */
	RUBY_T_ZOMBIE   = 0x1d, /*< @see struct ::RZombie */
	RUBY_T_MOVED    = 0x1e, /*< @see struct ::RMoved */
	
	RUBY_T_MASK     = 0x1f  /*< Bitmask of ::ruby_value_type. */
}

#[link(name = "ruby")]
extern "C"
{
	pub fn ruby_init();
	pub fn ruby_cleanup(exit_value: c_int) -> c_int;
	pub fn rb_array_len(a: VALUE) -> c_long;
	pub fn rb_ary_push(ary: VALUE, elem: VALUE) -> VALUE;
	pub fn rb_ary_entry(ary: VALUE, off: c_long) -> VALUE;
	pub fn rb_type(obj: VALUE) -> ruby_value_type;
	pub fn rb_str_new(ptr: *const std::ffi::c_char, len: std::ffi::c_long) -> VALUE;
	pub fn rb_str_freeze(str: VALUE) -> VALUE;
	pub fn rb_str_cat(dst: VALUE, src: *const std::ffi::c_char, srclen: std::ffi::c_long) -> VALUE;
}

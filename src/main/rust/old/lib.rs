// pub mod lua;
// pub mod lua_connector;
// pub mod wordexp;
pub mod ruby_connector;
pub mod raven_connector;
pub mod raven_cxx_connector;
pub mod runner;
pub mod repository;
mod vector_storage;
mod bit_indexing;
mod wordexp;

use std::io::Write;

#[cfg(test)] use std::io::Write;

#[derive(Debug)]
pub enum Compressed_vector_u32
{
	n_1 {values: [u32; 1]},
	n_2 {values: [u32; 2]},
	n_3 {values: [u32; 3]},
	n_4 {values: [u32; 4]},
	other {values: Vec<u32>},
}

impl Default for Compressed_vector_u32
{
	fn default() -> Self {Compressed_vector_u32::other {values: Default::default()}}
}

impl Compressed_vector_u32
{
	pub fn as_slice(&self) -> &[u32]
	{
		match self
		{
			Self::n_1 {values} => values,
			Self::n_2 {values} => values,
			Self::n_3 {values} => values,
			Self::n_4 {values} => values,
			Self::other {values} => values.as_slice(),
		}
	}
	
	pub fn is_empty(&self) -> bool
	{
		match self
		{
			Self::other {values} if values.is_empty() => true,
			_ => false,
		}
	}
	
	pub fn push(&mut self, value: u32)
	{
		match self
		{
			Self::other {values} if values.is_empty() =>
			{
				std::mem::replace(self, Self::n_1 {values: [value]});
			}
			Self::n_1 {values} => 
			{
				let values = *values;
				std::mem::replace(self, Self::n_2 {values: [values[0], value]});
			}
			Self::n_2 {values} => 
			{
				let values = *values;
				std::mem::replace(self, Self::n_3 {values: [values[0], values[1], value]});
			}
			Self::n_3 {values} => 
			{
				let values = *values;
				std::mem::replace(self, Self::n_4 {values: [values[0], values[1], values[2], value]});
			}
			Self::n_4 {values} =>
			{
				let values = *values;
				std::mem::replace(self, Self::other {values: vec![values[0], values[1], values[2], values[3], value]});
			}
			Self::other {values} =>
			{
				values.push(value);
			}
		};
	}
	
	pub fn debug_names<'t>(&'t self, repo: &'t Os_str_repository) -> impl std::fmt::Debug + 't
	{
		struct Result<'t>(&'t Compressed_vector_u32, &'t Os_str_repository);
		impl std::fmt::Debug for Result<'_>
		{
			fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
			{
				let mut it = self.0.as_slice().iter();
				if let Some(&next) = it.next()
				{
					write!(f, "{:?}", self.1.get(next as usize))?;
				}
				for &next in it
				{
					write!(f, ", {:?}", self.1.get(next as usize))?;
				}
				Ok(())
			}
		}
		Result(self, repo)
	}
}

impl AsRef<[u32]> for Compressed_vector_u32
{
	fn as_ref(&self) -> &[u32]
	{
		match self
		{
			Self::n_1 {values} => values,
			Self::n_2 {values} => values,
			Self::n_3 {values} => values,
			Self::n_4 {values} => values,
			Self::other {values} => values,
		}
	}
}

#[derive(Debug)]
pub struct Target_entry
{
	pub name: &'static std::ffi::OsStr,
	pub mtime: Option<std::time::SystemTime>,
	pub rule_id: usize,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Target_kind
{
	file,
	phony,
	directory,
	executable,
}

#[derive(Default, Debug)]
pub struct Os_str_repository
{
	pub mapping: std::collections::BTreeMap<Box<std::ffi::OsStr>, usize>,
	pub values: Vec<Target_entry>,
}

impl Os_str_repository
{
	pub const fn new() -> Self
	{
		Self
		{
			mapping: std::collections::BTreeMap::new(),
			values: Vec::new(),
		}
	}
	
	pub fn with_capacity(capacity: usize) -> Self
	{
		Self
		{
			mapping: std::collections::BTreeMap::new(),
			values: Vec::with_capacity(capacity),
		}
	}
	
	pub fn get(&self, index: usize) -> (Target_kind, &std::ffi::OsStr)
	{
		let bytes = self.values[index].name.as_encoded_bytes();
		let kind = match bytes[0]
		{
			b'.' => {Target_kind::file}
			b':' => {Target_kind::phony}
			b'e' => {Target_kind::executable}
			b'd' => {Target_kind::directory}
			_ => unreachable!("raven: repository contains string {:?}", unsafe {std::ffi::OsStr::from_encoded_bytes_unchecked(bytes)})
		};
		(kind, unsafe {std::ffi::OsStr::from_encoded_bytes_unchecked(&bytes[1 ..])})
	}
	
	pub fn alias_name(&mut self, key: std::ffi::OsString, value: usize)
	{
		let key = key.into_boxed_os_str();
		let name = unsafe
		{
			let ptr = std::ptr::addr_of!(*key);
			ptr.as_ref().unwrap()
		};
		self.mapping.insert(key, value);
		self.values[value].name = name;
	}
	
	pub fn find(&mut self, key: &std::ffi::OsStr) -> Option<usize>
	{
		self.mapping.get(key).copied()
	}
	
	pub fn insert(&mut self, key: std::ffi::OsString, rule_id: usize) -> usize
	{
		let value = key.into_boxed_os_str();
		let name = unsafe
		{
			let ptr = std::ptr::addr_of!(*value);
			ptr.as_ref().unwrap()
		};
		
		match self.mapping.entry(value)
		{
			std::collections::btree_map::Entry::Vacant(entry) =>
			{
				let result = self.values.len();
				self.values.push(Target_entry {name, mtime: None, rule_id});
				entry.insert(result);
				result
			}
			std::collections::btree_map::Entry::Occupied(entry) =>
			{
				let result = *entry.get();
				if rule_id != usize::MAX
				{
					self.values[result].rule_id = rule_id;
				}
				result
			}
		}
	}
	
	fn join_strings(prefix: &[u8], string: &[u8]) -> std::ffi::OsString
	{
		let mut result = Vec::with_capacity(prefix.len() + string.len());
		result.extend_from_slice(prefix);
		result.extend_from_slice(string);
		unsafe {std::ffi::OsString::from_encoded_bytes_unchecked(result)}
	}
}

impl std::ops::Index<usize> for Os_str_repository
{
	type Output = Target_entry;
	
	fn index(&self, index: usize) -> &Self::Output
	{
		&self.values[index]
	}
}

impl std::ops::IndexMut<usize> for Os_str_repository
{
	fn index_mut(&mut self, index: usize) -> &mut Self::Output
	{
		&mut self.values[index]
	}
}

pub fn min_opt_mtime(lhs: &Option<std::time::SystemTime>, rhs: &Option<std::time::SystemTime>) -> std::cmp::Ordering
{
	match (lhs, rhs)
	{
		(Some(ltime), Some(rtime)) => ltime.cmp(&rtime),
		(None, None) => std::cmp::Ordering::Equal,
		(None, _) => std::cmp::Ordering::Less,
		(_, None) => std::cmp::Ordering::Greater,
	}
}

pub fn remove_file_or_dir(path: impl AsRef<std::path::Path>) -> Result<bool, String>
{
	match std::fs::metadata(path.as_ref())
	{
		Err(error) if error.kind() == std::io::ErrorKind::NotFound => {Ok(false)}
		Err(error) =>
		{
			Err(format!("failed to obtain metadata for {:?}: {:?}", path.as_ref(), error))
		}
		Ok(metadata) => if let Err(error) = match metadata.is_dir()
		{
			true => {std::fs::remove_dir_all(path.as_ref())}
			false => {std::fs::remove_file(path.as_ref())}
		}
		{
			Err(format!("failed to remove {:?}: {:?}", path.as_ref(), error))
		}
		else
		{
			Ok(true)
		}
	}
}

#[derive(Debug, Default, PartialEq, Eq)]
pub enum Rule_dfs_state
{
	#[default]
	initial,
	discovered,
	finished,
}

#[derive(Debug)]
pub enum Recipe
{
	command {command: usize, args: String_vec},
	script {script: String},
}

#[derive(Debug, Default)]
pub struct Rule
{
	pub targets: Compressed_vector_u32,
	pub sources: Compressed_vector_u32,
	pub recipe: Option<Recipe>,
	pub implicit_directories: Option<bool>,
	pub env: std::collections::BTreeMap<std::ffi::OsString, std::ffi::OsString>,
	// After resolution
	pub dfs_state: Rule_dfs_state,
	pub sources_source: u32,
	pub sources_finished: u32,
	pub dependants: Compressed_vector_u32,
	pub is_leaf: bool,
	pub was_already_done: bool,
}

#[derive(Default)]
pub struct String_vec
{
	data: Vec<u8>,
	len: usize,
}

impl std::fmt::Debug for String_vec
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		write!(f, "[")?;
		let mut last = false;
		
		for string in self.iter()
		{
			if last
			{
				write!(f, ", ")?;
			}
			
			write!(f, "{:?}", string)?;
			last = true;
		}
		
		write!(f, "]")?;
		
		Ok(())
	}
}

pub struct String_vec_iterator<'t>
{
	data: &'t Vec<u8>,
	index: usize,
}

impl<'t> std::iter::Iterator for String_vec_iterator<'t>
{
	type Item = &'t str;
	
	fn next(&mut self) -> Option<Self::Item>
	{
		if self.index == self.data.len()
		{
			return None;
		}
		
		let len = u16::from_ne_bytes(self.data[self.index ..][.. 2].try_into().unwrap()) as usize;
		self.index += 2;
		let index = self.index;
		self.index += len;
		
		return Some(std::str::from_utf8(&self.data[index ..][.. len]).unwrap());
	}
}

pub struct String_vec_writer<'t>
{
	data: &'t mut Vec<u8>,
	index: usize,
}

impl<'t> std::io::Write for String_vec_writer<'t>
{
	fn write(&mut self, buf: &[u8]) -> std::io::Result<usize>
	{
		self.data.extend_from_slice(buf);
		let new_len = u16::from_ne_bytes(self.data[self.index ..][.. 2].try_into().unwrap()) + buf.len() as u16;
		self.data[self.index ..][.. 2].copy_from_slice(&u16::to_ne_bytes(new_len));
		Ok(buf.len())
	}
	
	fn flush(&mut self) -> std::io::Result<()>
	{
		Ok(())
	}
}

impl String_vec
{
	pub fn push(&mut self, string: &[u8])
	{
		self.data.extend_from_slice(&(string.len() as u16).to_ne_bytes());
		self.data.extend_from_slice(string);
		self.len += 1;
	}
	
	pub fn extend(&mut self, other: &String_vec)
	{
		self.data.extend_from_slice(other.data.as_slice());
		self.len += other.len;
	}
	
	/*
	pub fn push_unique(&mut self, string: &[u8])
	{
		if self.iter().find(|&self_string| self_string.as_bytes() == string).is_none()
		{
			self.push(string);
		}
	}
	
	pub fn extend_unique(&mut self, other: &String_vec)
	{
		for string in other.iter()
		{
			self.push_unique(string.as_bytes());
		}
	}
	*/
	
	pub fn remove(&mut self, string: &[u8]) -> bool
	{
		let mut it = self.iter();
		let mut last_index = 0;
		
		while let Some(self_string) = it.next()
		{
			if self_string.as_bytes() == string
			{
				self.data.drain(last_index .. last_index + 2 + string.len());
				self.len -= 1;
				return true;
			}
			
			last_index = it.index;
		}
		
		return false;
	}
	
	pub fn iter<'t>(&'t self) -> String_vec_iterator<'t>
	{
		String_vec_iterator {data: (&self.data).into(), index: 0}
	}
	
	pub fn get<'t>(&'t self, index: &mut usize) -> Option<&'t str>
	{
		let mut it = String_vec_iterator {data: &self.data, index: *index};
		let result = it.next();
		*index = it.index;
		return result;
	}
	
	pub fn push_writer<'t>(&'t mut self) -> String_vec_writer<'t>
	{
		self.push(b"");
		let index = self.data.len() - 2;
		String_vec_writer {data: &mut self.data, index}
	}
	
	pub fn is_empty(&self) -> bool
	{
		self.len == 0
	}
	
	pub fn len(&self) -> usize
	{
		self.len
	}
	
	pub fn clear(&mut self)
	{
		self.data.clear();
		self.len = 0;
	}
}

#[test]
fn test_string_vec()
{
	assert_eq!(0, String_vec::default().iter().count());
	
	{
		let mut sv = String_vec::default();
		sv.push(b"string");
		assert_eq!(1, sv.len());
		
		assert_eq!(1, sv.iter().count());
		assert_eq!("string", *sv.iter().last().as_ref().unwrap());
	}
	
	{
		let mut sv = String_vec::default();
		sv.push(b"1");
		sv.push(b"2");
		sv.push(b"3");
		sv.push(b"4");
		assert_eq!(4, sv.len());
		
		let mut it = sv.iter();
		assert_eq!("1", it.next().unwrap());
		assert_eq!("2", it.next().unwrap());
		assert_eq!("3", it.next().unwrap());
		assert_eq!("4", it.next().unwrap());
		assert_eq!(None, it.next());
	}
	
	{
		let mut sv = String_vec::default();
		let mut writer = sv.push_writer();
		writer.write(b"123").unwrap();
		assert_eq!(1, sv.len());
		
		let mut it = sv.iter();
		assert_eq!("123", it.next().unwrap());
		assert_eq!(None, it.next());
	}
}

#[test]
fn test_string_vec_remove()
{
	{
		let mut sv = String_vec::default();
		sv.push(b"1");
		sv.push(b"2");
		sv.push(b"3");
		sv.push(b"4");
		
		assert_eq!(true, sv.remove(b"2"));
		
		let mut it = sv.iter();
		assert_eq!("1", it.next().unwrap());
		assert_eq!("3", it.next().unwrap());
		assert_eq!("4", it.next().unwrap());
		assert_eq!(None, it.next());
		
		assert_eq!(false, sv.remove(b"2"));
	}
}

fn escape_backslash<'t>(string: &'t [u8]) -> Vec<u8>
{
	let mut result = Vec::with_capacity(string.len());
	let mut pos = 0;
	
	while pos != string.len()
	{
		if string[pos] == b'\\'
		{
			pos += 1;
			
			if pos != string.len()
			{
				if string[pos] == b'\\'
				{
					result.push(b'\\');
				}
				else
				{
					result.push(string[pos]);
				}
			}
		}
		else
		{
			result.push(string[pos]);
		}
		
		pos += 1;
	}
	
	result
}

fn escape_newlines<'t>(string: &'t mut [u8]) -> &'t mut [u8]
{
	let mut read = 0;
	let mut write = 0;
	let read_len = string.len();
	
	while read != read_len
	{
		if string[read ..].starts_with(b"\\\n")
		{
			read += 2;
		}
		else
		{
			string[write] = string[read];
			read += 1;
			write += 1;
		}
	}
	
	return &mut string[.. write];
}

#[test]
fn test_escape_newlines()
{
	assert_eq!(b"", escape_newlines(b"".to_vec().as_mut_slice()));
	assert_eq!(b"\\", escape_newlines(b"\\".to_vec().as_mut_slice()));
	assert_eq!(b"", escape_newlines(b"\\\n".to_vec().as_mut_slice()));
	assert_eq!(b"a: b c", escape_newlines(b"a: b\\\n c".to_vec().as_mut_slice()));
	assert_eq!(b"a: b c\nd", escape_newlines(b"a: b\\\n c\nd".to_vec().as_mut_slice()));
}

fn skip_whitespace(string: &mut &[u8]) -> bool
{
	let mut result = false;
	
	loop
	{
		match string.get(0)
		{
			None =>
			{
				return false;
			}
			Some(b'\n') =>
			{
				result = true;
				*string = &mut &string[1 ..];
			}
			Some(b) if b.is_ascii_whitespace() =>
			{
				*string = &mut &string[1 ..];
			}
			_ =>
			{
				return result;
			}
		}
	}
}

#[test]
fn test_skip_whitespace()
{
	let mut string: &mut &[u8] = &mut [0; 0].as_slice();
	
	*string = b"";
	assert_eq!(false, skip_whitespace(&mut string));
	assert_eq!(b"", string);
	
	*string = b" a";
	assert_eq!(false, skip_whitespace(&mut string));
	assert_eq!(b"a", string);
	
	*string = b"\na";
	assert_eq!(true, skip_whitespace(&mut string));
	assert_eq!(b"a", string);
	
	*string = b"\n a";
	assert_eq!(true, skip_whitespace(&mut string));
	assert_eq!(b"a", string);
	
	*string = b" \n a";
	assert_eq!(true, skip_whitespace(&mut string));
	assert_eq!(b"a", string);
}

fn read_target<'t>(string: &'t [u8], writer: &mut impl std::io::Write) -> &'t [u8]
{
	let mut end = 0;
	
	while end != string.len()
	{
		if string[end] == b':'
		{
			end += 1;
			
			let Some(&b) = string.get(end) else {break};
			
			if b.is_ascii_whitespace()
			{
				break;
			}
			
			writer.write(std::slice::from_ref(&string[end - 1])).unwrap();
		}
		else if string[end] == b'\\'
		{
			let Some(&b) = string.get(end + 1) else
			{
				writer.write(std::slice::from_ref(&string[end]));
				break
			};
			
			if ! b.is_ascii_whitespace()
			{
				writer.write(std::slice::from_ref(&string[end]));
			}
			
			writer.write(std::slice::from_ref(&b));
			end += 2;
		}
		else
		{
			writer.write(std::slice::from_ref(&string[end]));
			end += 1;
		}
	}
	
	return &string[end ..];
}

#[test]
fn test_read_target()
{
	let mut result = Vec::with_capacity(32);
	
	assert_eq!(b"", read_target(b"", &mut result));
	assert_eq!(b"", result.as_slice());
	result.clear();
	
	assert_eq!(b"", read_target(b"a:", &mut result));
	assert_eq!(b"a", result.as_slice());
	result.clear();
	
	assert_eq!(b" b", read_target(b"a: b", &mut result));
	assert_eq!(b"a", result.as_slice());
	result.clear();
	
	assert_eq!(b"", read_target(b"a:b", &mut result));
	assert_eq!(b"a:b", result.as_slice());
	result.clear();
	
	assert_eq!(b"", read_target(b"a:\\ b", &mut result));
	assert_eq!(b"a: b", result.as_slice());
	result.clear();
	
	assert_eq!(b"", read_target(b"a:\\ b\\ c", &mut result));
	assert_eq!(b"a: b c", result.as_slice());
	result.clear();
}

fn read_prerequisite<'t>(string: &'t [u8], writer: &mut impl std::io::Write) -> &'t [u8]
{
	let mut end = 0;
	
	while end != string.len()
	{
		if string[end].is_ascii_whitespace()
		{
			break;
		}
		else if string[end] == b'\\'
		{
			let Some(&b) = string.get(end + 1) else
			{
				writer.write(std::slice::from_ref(&string[end]));
				break
			};
			
			if ! b.is_ascii_whitespace()
			{
				writer.write(std::slice::from_ref(&string[end]));
			}
			
			writer.write(std::slice::from_ref(&b));
			end += 2;
		}
		else
		{
			writer.write(std::slice::from_ref(&string[end]));
			end += 1;
		}
	}
	
	return &string[end ..];
}

#[test]
fn test_read_prerequisite()
{
	let mut result = Vec::with_capacity(32);
	
	assert_eq!(b"", read_prerequisite(b"", &mut result));
	assert_eq!(b"", result.as_slice());
	result.clear();
	
	assert_eq!(b"", read_prerequisite(b"a", &mut result));
	assert_eq!(b"a", result.as_slice());
	result.clear();
	
	assert_eq!(b" b", read_prerequisite(b"a b", &mut result));
	assert_eq!(b"a", result.as_slice());
	result.clear();
	
	assert_eq!(b"", read_prerequisite(b"a\\ b", &mut result));
	assert_eq!(b"a b", result.as_slice());
	result.clear();
}

pub fn read_make(stream: &mut impl std::io::Read) -> std::io::Result<Vec<(String, String_vec)>>
{
	let mut result = Vec::with_capacity(16);
	
	let mut text = Vec::with_capacity(128);
	stream.read_to_end(&mut text)?;
	let text = escape_newlines(text.as_mut_slice());
	let mut text = &*text;
	
	while ! text.is_empty()
	{
		let mut target_writer = Vec::with_capacity(64);
		text = read_target(text, &mut target_writer);
		
		if target_writer.is_empty() || text.is_empty()
		{
			break;
		}
		
		if ! text.starts_with(b" ")
		{
			continue;
		}
		
		text = &text[1 ..];
		skip_whitespace(&mut text);
		
		let mut prerequisites = String_vec::default();
		let mut writer = prerequisites.push_writer();
		text = read_prerequisite(text, &mut writer);
		
		while ! text.is_empty() && ! skip_whitespace(&mut text)
		{
			writer = prerequisites.push_writer();
			text = read_prerequisite(text, &mut writer);
		}
		
		result.push((String::from_utf8(target_writer).unwrap(), prerequisites));
	}
	
	Ok(result)
}

#[test]
fn test_read_make()
{
}

fn c_dependency_file_for_core(target: &[u8]) -> Vec<u8>
{
	let path = std::path::PathBuf::from(std::str::from_utf8(target).unwrap());
	path.parent().unwrap().parent().unwrap().join("dependencies").join(path.file_name().unwrap()).with_extension("mk").into_os_string().into_string().unwrap().into_bytes()
}
#[test]
fn test_c_dependency_file_for_core()
{
	assert_eq!(b"target/dependencies/main.mk", c_dependency_file_for_core(b"target/object_files/main.o").as_slice());
}

fn read_backslash(string: &[u8], mut pos: usize, writer: &mut impl std::io::Write) -> Result<usize, String>
{
	pos += 1;
	let Some(&b) = string.get(pos) else
	{
		return Err(format!("expand: unterminated backslash"));
	};
	pos += 1;
	writer.write(std::slice::from_ref(&b)).unwrap();
	return Ok(pos);
}

#[test]
fn test_read_backslash()
{
	let mut writer = Vec::with_capacity(32);
	
	assert!(read_backslash(b"\\", 0, &mut writer).is_err());
	writer.clear();
	assert!(read_backslash(b" \\", 1, &mut writer).is_err());
	writer.clear();
	assert_eq!(Ok(2), read_backslash(b"\\\\", 0, &mut writer));
	assert_eq!(b"\\", writer.as_slice());
	writer.clear();
	assert_eq!(Ok(2), read_backslash(b"\\a", 0, &mut writer));
	assert_eq!(b"a", writer.as_slice());
	writer.clear();
}

fn read_single_quoted(string: &[u8], mut pos: usize, writer: &mut impl std::io::Write) -> Result<usize, String>
{
	pos += 1;
	let old_pos = pos;
	while let Some(&b) = string.get(pos)
	{
		pos += 1;
		if b == b'\''
		{
			writer.write(&string[old_pos .. pos - 1]).unwrap();
			return Ok(pos);
		}
	}
	
	return Err(format!("expand: unterminated single quote"));
}

#[test]
fn test_read_single_quoted()
{
	let mut writer = Vec::with_capacity(32);
	
	assert!(read_single_quoted(b"'", 0, &mut writer).is_err());
	writer.clear();
	assert!(read_single_quoted(b"' ", 0, &mut writer).is_err());
	writer.clear();
	assert!(read_single_quoted(b" ' ", 1, &mut writer).is_err());
	writer.clear();
	assert_eq!(Ok(3), read_single_quoted(b"'a'", 0, &mut writer));
	assert_eq!(b"a", writer.as_slice());
	writer.clear();
	assert_eq!(Ok(5), read_single_quoted(b"'a b'", 0, &mut writer));
	assert_eq!(b"a b", writer.as_slice());
	writer.clear();
	assert_eq!(Ok(5), read_single_quoted(b"'a\\b'", 0, &mut writer));
	assert_eq!(b"a\\b", writer.as_slice());
	writer.clear();
	assert_eq!(Ok(3), read_single_quoted(b"'\\''", 0, &mut writer));
	assert_eq!(b"\\", writer.as_slice());
	writer.clear();
}

fn read_double_quoted(string: &[u8], mut pos: usize, writer: &mut impl std::io::Write) -> Result<usize, String>
{
	pos += 1;
	let mut old_pos = pos;
	while let Some(&b) = string.get(pos)
	{
		if b == b'\"'
		{
			pos += 1;
			writer.write(&string[old_pos .. pos - 1]).unwrap();
			return Ok(pos);
		}
		else if b == b'\\'
		{
			writer.write(&string[old_pos .. pos]).unwrap();
			pos = read_backslash(string, pos, writer)?;
			old_pos = pos;
		}
		else
		{
			pos += 1;
		}
	}
	
	return Err(format!("expand: unterminated double quote"));
}

#[test]
fn test_read_double_quoted()
{
	let mut writer = Vec::with_capacity(32);
	
	assert!(read_double_quoted(b"\"", 0, &mut writer).is_err());
	writer.clear();
	assert!(read_double_quoted(b"\" ", 0, &mut writer).is_err());
	writer.clear();
	assert!(read_double_quoted(b" \" ", 1, &mut writer).is_err());
	writer.clear();
	assert_eq!(Ok(3), read_double_quoted(b"\"a\"", 0, &mut writer));
	assert_eq!(b"a", writer.as_slice());
	writer.clear();
	assert_eq!(Ok(5), read_double_quoted(b"\"a b\"", 0, &mut writer));
	assert_eq!(b"a b", writer.as_slice());
	writer.clear();
	assert_eq!(Ok(5), read_double_quoted(b"\"a\\b\"", 0, &mut writer));
	assert_eq!(b"ab", writer.as_slice());
	writer.clear();
	assert_eq!(Ok(4), read_double_quoted(b"\"\\\"\"", 0, &mut writer));
	assert_eq!(b"\"", writer.as_slice());
	writer.clear();
	assert_eq!(Ok(2), read_double_quoted(b"\"\"\"a\"\"\"", 0, &mut writer));
	assert_eq!(b"", writer.as_slice());
	assert_eq!(Ok(5), read_double_quoted(b"\"\"\"a\"\"\"", 2, &mut writer));
	assert_eq!(b"a", writer.as_slice());
	writer.clear();
}

pub fn expand(string: &[u8], output: &mut String_vec) -> Result<(), String>
{
	use std::io::Write;
	
	let mut pos = 0;
	
	while pos != string.len()
	{
		if string[pos].is_ascii_whitespace()
		{
			pos += 1;
			continue;
		}
		
		let mut writer = output.push_writer();
		let orig_pos = pos;
		let mut old_pos = pos;
		while pos != string.len() && ! string[pos].is_ascii_whitespace()
		{
			let prev_pos = pos;
			if string[pos] == b'\''
			{
				writer.write(&string[old_pos .. pos]).unwrap();
				pos = read_single_quoted(string, pos, &mut writer)?;
				old_pos = pos;
				if prev_pos + 2 == pos && pos - orig_pos == 2
				{
					break;
				}
				continue;
			}
			
			if string[pos] == b'\"'
			{
				writer.write(&string[old_pos .. pos]).unwrap();
				pos = read_double_quoted(string, pos, &mut writer)?;
				old_pos = pos;
				if prev_pos + 2 == pos && pos - orig_pos == 2
				{
					break;
				}
				continue;
			}
			
			if string[pos] == b'\\'
			{
				writer.write(&string[old_pos .. pos]).unwrap();
				pos = read_backslash(string, pos, &mut writer)?;
				old_pos = pos;
			}
			else
			{
				pos += 1;
			}
		}
		
		writer.write(&string[old_pos .. pos]).unwrap();
	}
	
	Ok(())
}

#[test]
fn test_expand()
{
	let mut sv = String_vec::default();
	
	assert!(expand(b"", &mut sv).is_ok());
	assert_eq!(0, sv.len());
	sv.clear();
	
	assert!(expand(b"-Isrc", &mut sv).is_ok());
	assert_eq!(1, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("-Isrc"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"-Isrc -Llib", &mut sv).is_ok());
	assert_eq!(2, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("-Isrc"), it.next());
	assert_eq!(Some("-Llib"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"one 'two args' \"three\"", &mut sv).is_ok());
	assert_eq!(3, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("one"), it.next());
	assert_eq!(Some("two args"), it.next());
	assert_eq!(Some("three"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\\\\", &mut sv).is_ok());
	assert_eq!(1, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("\\"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\"\\\\\"", &mut sv).is_ok());
	assert_eq!(1, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("\\"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\'\\\\\'", &mut sv).is_ok());
	assert_eq!(1, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("\\\\"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\'\'\'\'", &mut sv).is_ok());
	assert_eq!(2, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some(""), it.next());
	assert_eq!(Some(""), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\\b", &mut sv).is_ok());
	assert_eq!(1, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("b"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"a\"b\"", &mut sv).is_ok());
	assert_eq!(1, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("ab"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\"\"\"a\"\"\"", &mut sv).is_ok());
	assert_eq!(2, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some(""), it.next());
	assert_eq!(Some("a"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\'\'\'a\'\'\'", &mut sv).is_ok());
	assert_eq!(2, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some(""), it.next());
	assert_eq!(Some("a"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\"\\\"\"", &mut sv).is_ok());
	assert_eq!(1, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("\""), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"a\"\"", &mut sv).is_ok());
	assert_eq!(1, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("a"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\"\"b", &mut sv).is_ok());
	assert_eq!(2, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some(""), it.next());
	assert_eq!(Some("b"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\"\"\"\"b", &mut sv).is_ok());
	assert_eq!(3, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some(""), it.next());
	assert_eq!(Some(""), it.next());
	assert_eq!(Some("b"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"a\"\"b", &mut sv).is_ok());
	assert_eq!(1, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some("ab"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
	
	assert!(expand(b"\"\"a\"\"b", &mut sv).is_ok());
	assert_eq!(2, sv.len());
	let mut it = sv.iter();
	assert_eq!(Some(""), it.next());
	assert_eq!(Some("ab"), it.next());
	assert_eq!(None, it.next());
	sv.clear();
}

pub struct Expander<'t>
{
	string: &'t[u8],
	pos: usize,
}

impl<'t, Type> From<&'t Type> for Expander<'t>
where Type: AsRef<[u8]>
{
	fn from(value: &'t Type) -> Self
	{
		Self {string: value.as_ref(), pos: 0}
	}
}

impl Iterator for Expander<'_>
{
	type Item = Result<Vec<u8>, String>;
	
	fn next(&mut self) -> Option<Self::Item>
	{
		use std::io::Write;
		
		while self.pos != self.string.len()
		{
			if self.string[self.pos].is_ascii_whitespace()
			{
				self.pos += 1;
				continue;
			}
			
			let mut writer = Vec::<u8>::with_capacity(16);
			
			while self.pos != self.string.len() && ! self.string[self.pos].is_ascii_whitespace()
			{
				let old_pos = self.pos;
				if self.string[self.pos] == b'\''
				{
					self.pos = match read_single_quoted(self.string, self.pos, &mut writer)
					{
						Ok(pos) => pos,
						Err(err) => return Some(Err(err)),
					};
					if old_pos + 2 == self.pos
					{
						break;
					}
					continue;
				}
				
				if self.string[self.pos] == b'\"'
				{
					self.pos = match read_double_quoted(self.string, self.pos, &mut writer)
					{
						Ok(pos) => pos,
						Err(err) => return Some(Err(err)),
					};
					if old_pos + 2 == self.pos
					{
						break;
					}
					continue;
				}
				
				if self.string[self.pos] == b'\\'
				{
					self.pos = match read_backslash(self.string, self.pos, &mut writer)
					{
						Ok(pos) => pos,
						Err(err) => return Some(Err(err)),
					};
				}
				else
				{
					writer.write(std::slice::from_ref(&self.string[self.pos])).unwrap();
					self.pos += 1;
				}
			}
			
			return Some(Ok(writer));
		}
		
		return None;
	}
}

#[test]
fn test_expander()
{
	assert!(Expander::from(b"").next().is_none());
	
	{
		let mut it = Expander::from(b"-Isrc");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"-Isrc", v.as_slice());
		assert_eq!(None, it.next());
	}
	{
		let mut it = Expander::from(b"-Isrc -Llib");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"-Isrc", v.as_slice());
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"-Llib", v.as_slice());
		assert_eq!(None, it.next());
	}
	{
		let mut it = Expander::from(b"one 'two args' \"three\"");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"one", v.as_slice());
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"two args", v.as_slice());
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"three", v.as_slice());
		assert_eq!(None, it.next());
	}
	{
		let mut it = Expander::from(b"\\\\");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"\\", v.as_slice());
		assert_eq!(None, it.next());
	}
	{
		let mut it = Expander::from(b"\"\\\\\"");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"\\", v.as_slice());
		assert_eq!(None, it.next());
	}
	{
		let mut it = Expander::from(b"\'\\\\\'");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"\\\\", v.as_slice());
		assert_eq!(None, it.next());
	}
	{
		let mut it = Expander::from(b"\'\'\'\'");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"", v.as_slice());
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"", v.as_slice());
		assert_eq!(None, it.next());
	}
	{
		let mut it = Expander::from(b"\\b");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"b", v.as_slice());
		assert_eq!(None, it.next());
	}
	{	
		let mut it = Expander::from(b"a\"b\"");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"ab", v.as_slice());
		assert_eq!(None, it.next());
	}
	{	
		let mut it = Expander::from(b"\"\"\"a\"\"\"");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"", v.as_slice());
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"a", v.as_slice());
		assert_eq!(None, it.next());
	}
	{	
		let mut it = Expander::from(b"\'\'\'a\'\'\'");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"", v.as_slice());
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"a", v.as_slice());
		assert_eq!(None, it.next());
	}
	{	
		let mut it = Expander::from(b"\"\\\"\"");
		let Some(Ok(v)) = it.next() else {panic!()};
		assert_eq!(b"\"", v.as_slice());
		assert_eq!(None, it.next());
	}
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Stack_type
{
	initial,
	finished,
}

#[derive(Clone, Copy, Debug)]
pub struct Stack_entry
{
	pub index: usize,
	pub kind: Stack_type,
}

use crate::vector_storage::Vector_storage;
use crate::bit_indexing;

pub struct Repository<Type>
{
	storage: Vector_storage,
	len: usize,
	index_length: usize,
	_data: std::marker::PhantomData<Type>,
}

unsafe impl<Type: Send> Send for Repository<Type> {}
unsafe impl<Type: Sync> Sync for Repository<Type> {}

impl<Type> Repository<Type>
{
	pub fn new() -> Self
	{
		assert_ne!(0, std::mem::size_of::<Type>(), "zero-sized type");
		
		Self
		{
			storage: Vector_storage::new(),
			len: 0,
			index_length: 0,
			_data: std::marker::PhantomData,
		}
	}
	
	pub fn with_capacity(capacity: usize) -> Self
	{
		let mut result = Self::new();
		result.reserve(capacity);
		return result;
	}
	
	pub fn len(&self) -> usize {self.len}
	pub fn is_empty(&self) -> bool {self.len == 0}
	
	pub fn values(&self) -> &[Type]
	{
		unsafe {std::slice::from_raw_parts(self.storage.data.as_ptr()
			.offset(Self::array_offset(self.index_length) as isize).cast::<Type>(), self.storage.capacity)
		}
	}
	
	pub fn values_mut(&mut self) -> &mut [Type]
	{
		unsafe {std::slice::from_raw_parts_mut(self.storage.data.as_ptr()
			.offset(Self::array_offset(self.index_length) as isize).cast::<Type>(), self.storage.capacity)
		}
	}
	
	fn simple_clear(&mut self)
	{
		let array_offset = Self::array_offset(self.index_length);
		
		for i in self.index_iter()
		{
			unsafe {self.storage.data.as_ptr().offset(array_offset as isize)
				.cast::<Type>().offset(i as isize).drop_in_place();
			};
		}
		
		for i in 0 .. self.index_length
		{
			unsafe {self.storage.data.as_ptr().cast::<bit_indexing::Index_type>()
				.offset(i as isize).drop_in_place()
			};
		}
	}
	
	pub fn clear(&mut self)
	{
		if self.storage.capacity != 0
		{
			self.simple_clear();
			self.len = 0;
		}
	}
	
	fn array_offset(index_length: usize) -> usize
	{
		let type_alignment = std::mem::align_of::<Type>();
		return (index_length * std::mem::size_of::<bit_indexing::Index_type>() as usize
			+ type_alignment - 1) / type_alignment * type_alignment;
	}
	
	fn layout_for(capacity: usize) -> (std::alloc::Layout, usize)
	{
		let type_alignment = std::mem::align_of::<Type>();
		let alignment = std::cmp::max(type_alignment,
			std::mem::align_of::<bit_indexing::Index_type>(),
		);
		let index_length = bit_indexing::index_length(capacity);
		let byte_size = Self::array_offset(index_length) + std::mem::size_of::<Type>() * capacity;
		
		return (std::alloc::Layout::from_size_align(byte_size, alignment).unwrap(), index_length);
	}
	
	fn index_header(&self) -> &[bit_indexing::Index_type]
	{
		unsafe {std::slice::from_raw_parts(
			self.storage.data.as_ptr().cast::<u128>(), self.index_length
		)}
	}
	
	fn index_header_mut(&mut self) -> &mut [bit_indexing::Index_type]
	{
		unsafe {std::slice::from_raw_parts_mut(
			self.storage.data.as_ptr().cast::<u128>(), self.index_length
		)}
	}
	
	pub fn index_iter(&self) -> bit_indexing::Index_slice_iterator
	{
		bit_indexing::Index_slice_iterator::new(&self.index_header()[
			self.index_length - bit_indexing::level_length(self.storage.capacity) .. self.index_length
		])
	}
	
	pub fn reserve(&mut self, capacity: usize)
	{
		if capacity <= self.storage.capacity
		{
			return;
		}
		
		let (new_layout, index_length) = Self::layout_for(capacity);
		
		let new_data = match std::ptr::NonNull::new(unsafe {std::alloc::alloc(new_layout)})
		{
			Some(p) => p,
			None => std::alloc::handle_alloc_error(new_layout),
		};
		
		for i in 0 .. index_length
		{
			unsafe {(new_data.as_ptr().cast::<bit_indexing::Index_type>()).offset(i as isize).write(0)};
		}
		
		if self.storage.capacity != 0
		{
			unsafe
			{
				bit_indexing::copy(
					std::slice::from_raw_parts(self.storage.data.as_ptr().cast::<u128>(), self.index_length),
					self.storage.capacity,
					std::slice::from_raw_parts_mut(new_data.as_ptr().cast::<u128>(), index_length),
					capacity,
				)
			};
			
			let array_offset = Self::array_offset(self.index_length);
			let new_array_offset = Self::array_offset(index_length);
			
			for i in self.index_iter()
			{
				unsafe
				{
					self.storage.data.as_ptr().offset(array_offset as isize).cast::<Type>()
					.offset(i as isize).copy_to_nonoverlapping(new_data.as_ptr().offset(new_array_offset as isize)
					.cast::<Type>().offset(i as isize), 1);
				};
			}
			
			unsafe {std::alloc::dealloc(self.storage.data.as_ptr(), Self::layout_for(self.storage.capacity).0)};
		}
		
		self.storage.data = new_data;
		self.storage.capacity = capacity;
		self.index_length = index_length;
	}
	
	pub fn insert(&mut self, value: Type) -> usize
	{
		if self.len == self.storage.capacity
		{
			self.reserve(Vector_storage::default_capacity_growth(self.storage.capacity));
		}
		
		let capacity = self.storage.capacity;
		let index = bit_indexing::push_front(self.index_header_mut(), capacity);
		
		unsafe {self.storage.data.as_ptr().offset(Self::array_offset(self.index_length) as isize)
			.cast::<Type>().offset(index as isize).write(value)
		};
		
		self.len += 1;
		
		return index;
	}
	
	pub fn take(&mut self, index: usize) -> Type
	{
		let capacity = self.storage.capacity;
		
		if ! bit_indexing::erase(self.index_header_mut(), index, capacity)
		{
			panic!("Attempting to erase a void value");
		}
		
		let result = unsafe {self.storage.data.as_ptr().offset(Self::array_offset(self.index_length) as isize)
			.cast::<Type>().offset(index as isize).read()
		};
		
		self.len -= 1;
		
		return result;
	}
	
	pub fn erase(&mut self, index: usize)
	{
		let capacity = self.storage.capacity;
		
		if ! bit_indexing::erase(self.index_header_mut(), index, capacity)
		{
			panic!("Attempting to erase a void value");
		}
		
		unsafe {self.storage.data.as_ptr().offset(Self::array_offset(self.index_length) as isize)
			.cast::<Type>().offset(index as isize).drop_in_place()
		};
		
		self.len -= 1;
	}
}

impl<Type> Drop for Repository<Type>
{
	fn drop(&mut self)
	{
		if self.storage.capacity != 0
		{
			self.simple_clear();
			unsafe {std::alloc::dealloc(self.storage.data.as_ptr(), Self::layout_for(self.storage.capacity).0)};
		}
	}
}

impl<Type> std::ops::Index<usize> for Repository<Type>
{
	type Output = Type;
	
	fn index(&self, index: usize) -> &Self::Output
	{
		&self.values()[index]
	}
}

impl<Type> std::ops::IndexMut<usize> for Repository<Type>
{
	fn index_mut(&mut self, index: usize) -> &mut Self::Output
	{
		&mut self.values_mut()[index]
	}
}

#[no_mangle]
pub unsafe extern "C" fn Raven_rst_cxx_get_arch(compiler: *const std::ffi::c_char, result: crate::ruby_connector::VALUE)
{
	let mut command = std::process::Command::new(std::ffi::OsStr::from_encoded_bytes_unchecked(std::ffi::CStr::from_ptr(compiler).to_bytes()));
	command.arg("-dumpmachine");
	let output = command.output().unwrap().stdout;
	crate::ruby_connector::rb_str_cat(result, output.as_slice().as_ptr().cast(), output.as_slice().len() as std::ffi::c_long - 1);
}

#[no_mangle]
pub unsafe extern "C" fn Raven_cxx_rst_get_header_dependencies(source_file: crate::raven_connector::String_view, result: crate::ruby_connector::VALUE)
{
	let mut cmd = std::process::Command::new("g++");
	cmd.arg(std::ffi::OsStr::from_encoded_bytes_unchecked(source_file.as_ref()));
	cmd.args(["-E", "-H", "-o", "/dev/null"].iter());
	cmd.arg("-Isrc");
	// cmd.args(args);
	let output = cmd.output().unwrap();
	if output.status.success()
	{
		let text = std::str::from_utf8(&output.stderr).unwrap();
		for mut line in text.lines()
		{
			if line.starts_with(".")
			{
				while line.as_bytes()[0] == b'.'
				{
					line = &line[1 ..];
				}
				line = &line[1 ..];
				
				let path = std::path::PathBuf::from(line).canonicalize().unwrap();
				let bytes = path.as_os_str().as_encoded_bytes();
				let value = crate::ruby_connector::rb_str_new(bytes.as_ptr().cast(), bytes.len() as std::ffi::c_long);
				crate::ruby_connector::rb_ary_push(result, value);
			}
		}
	}
}

pub fn get_header_dependencies<I, S>(path: &std::path::Path, args: I)
where I: IntoIterator<Item = S>, S: AsRef<std::ffi::OsStr>
{
	let mut cmd = std::process::Command::new("g++");
	cmd.arg(path.as_os_str());
	cmd.args(["-E", "-H", "-o", "/dev/null"].iter());
	cmd.args(args);
	let output = cmd.output().unwrap();
	if output.status.success()
	{
		let text = std::str::from_utf8(&output.stderr).unwrap();
		for mut line in text.lines()
		{
			if line.starts_with(".")
			{
				line = &line[1 ..];
				while line.as_bytes()[0] != b'/'
				{
					line = &line[1 ..];
				}
				println!("{:?}", std::path::PathBuf::from(line).canonicalize());
			}
		}
	}
}

use std::ffi::*;

// Flags
const WRDE_DOOFFS: c_int = 1 << 0; // Insert PWORDEXP->we_offs NULLs
const WRDE_APPEND: c_int = 1 << 1; // Append to results of a previous call
const WRDE_NOCMD: c_int = 1 << 2; // Don't do command substitution
const WRDE_REUSE: c_int = 1 << 3; // Reuse storage in PWORDEXP
const WRDE_SHOWERR: c_int = 1 << 4; // Don't redirect stderr to /dev/null
const WRDE_UNDEF: c_int = 1 << 5; // Error for expanding undefined variables

// Result
const WRDE_NOSYS: c_int = -1; // Never used since we support `wordexp'
const WRDE_NOSPACE: c_int = 1; // Ran out of memory
const WRDE_BADCHAR: c_int = 2; // A metachar appears in the wrong place
const WRDE_BADVAL: c_int = 3; // Undefined var reference with WRDE_UNDEF
const WRDE_CMDSUB: c_int = 4; // Command substitution with WRDE_NOCMD
const WRDE_SYNTAX: c_int = 5; // Shell syntax error

// Structure describing a word-expansion run
#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct wordexp_t
{
	we_wordc: usize, // Count of words matched
	we_wordv: *mut *mut c_char, // List of expanded words
	we_offs: usize, // Slots to reserve in `we_wordv'
}

impl Default for wordexp_t
{
	fn default() -> Self
	{
		unsafe {std::mem::zeroed()}
	}
}

#[link(name = "c")]
extern "C"
{
	// Do word expansion of WORDS into PWORDEXP
	fn wordexp(words: *const c_char, pwordexp: *mut wordexp_t, flags: c_int) -> c_int;
	
	// Free the storage allocated by a `wordexp' call
	fn wordfree(wordexp: *mut wordexp_t);
}

pub struct Wordexp_iterator<'t>
{
	object: wordexp_t,
	index: isize,
	wordexp: std::marker::PhantomData<&'t Wordexp>,
}

impl<'t> Iterator for Wordexp_iterator<'t>
{
	type Item = &'t std::ffi::OsStr;
	
	fn next(&mut self) -> Option<Self::Item>
	{
		if self.index as usize == self.object.we_wordc
		{
			return None;
		}
		
		let result = unsafe
		{
			std::ffi::CStr::from_ptr(*self.object.we_wordv.offset(self.index))
		};
		
		self.index += 1;
		
		return Some(unsafe {std::ffi::OsStr::from_encoded_bytes_unchecked(result.to_bytes())});
	}
}

#[derive(Debug)]
pub struct Wordexp
{
	object: wordexp_t,
}

impl Drop for Wordexp
{
	fn drop(&mut self)
	{
		unsafe {wordfree(&mut self.object)};
	}
}

impl Wordexp
{
	pub fn new(words: &std::ffi::CStr) -> Result<Self, String>
	{
		let mut object = wordexp_t::default();
		match unsafe {wordexp(words.as_ptr(), &mut object, WRDE_NOCMD | WRDE_UNDEF)}
		{
			0 => {Ok(Self {object})}
			WRDE_NOSPACE => {Err(format!("wordexp: out of memory"))}
			WRDE_BADCHAR => {Err(format!("wordexp: a metachar appears in the wrong place"))}
			WRDE_BADVAL => {Err(format!("wordexp: undefined var reference with WRDE_UNDEF"))}
			WRDE_CMDSUB => {Err(format!("wordexp: command substitution with WRDE_NOCMD"))}
			WRDE_SYNTAX => {Err(format!("wordexp: shell syntax error"))}
			error => {Err(format!("wordexp returned invalid error code: {}", error))}
		}
	}
	
	pub fn len(&self) -> usize
	{
		return self.object.we_wordc;
	}
	
	pub fn iter<'t>(&'t self) -> Wordexp_iterator<'t>
	{
		Wordexp_iterator {object: self.object, index: 0, wordexp: Default::default()}
	}
}

#[test]
fn test_wordexp()
{
	let mut wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\0").unwrap()).unwrap();
	assert_eq!(0, wordexp.len());
	assert_eq!(0, wordexp.iter().count());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"-Isrc\0").unwrap()).unwrap();
	assert_eq!(1, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("-Isrc".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"-Isrc -Llib\0").unwrap()).unwrap();
	assert_eq!(2, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("-Isrc".as_ref()), it.next());
	assert_eq!(Some("-Llib".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"one 'two args' \"three\"\0").unwrap()).unwrap();
	assert_eq!(3, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("one".as_ref()), it.next());
	assert_eq!(Some("two args".as_ref()), it.next());
	assert_eq!(Some("three".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\\\\\0").unwrap()).unwrap();
	assert_eq!(1, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("\\".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\"\\\\\"\0").unwrap()).unwrap();
	assert_eq!(1, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("\\".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\'\\\\\'\0").unwrap()).unwrap();
	assert_eq!(1, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("\\\\".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\'\'\'\'\0").unwrap()).unwrap();
	assert_eq!(2, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("".as_ref()), it.next());
	assert_eq!(Some("".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\\b\0").unwrap()).unwrap();
	assert_eq!(1, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("b".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"a\"b\"\0").unwrap()).unwrap();
	assert_eq!(1, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("ab".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\"\"\"a\"\"\"\0").unwrap()).unwrap();
	assert_eq!(2, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("".as_ref()), it.next());
	assert_eq!(Some("a".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\'\'\'a\'\'\'\0").unwrap()).unwrap();
	assert_eq!(2, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("".as_ref()), it.next());
	assert_eq!(Some("a".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\"\\\"\"\0").unwrap()).unwrap();
	assert_eq!(1, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("\"".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"a\"\"\0").unwrap()).unwrap();
	assert_eq!(1, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("a".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\"\"b\0").unwrap()).unwrap();
	assert_eq!(2, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("".as_ref()), it.next());
	assert_eq!(Some("b".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\"\"\"\"b\0").unwrap()).unwrap();
	assert_eq!(3, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("".as_ref()), it.next());
	assert_eq!(Some("".as_ref()), it.next());
	assert_eq!(Some("b".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"a\"\"b\0").unwrap()).unwrap();
	assert_eq!(1, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("ab".as_ref()), it.next());
	assert_eq!(None, it.next());
	
	wordexp = Wordexp::new(std::ffi::CStr::from_bytes_with_nul(b"\"\"a\"\"b\0").unwrap()).unwrap();
	assert_eq!(2, wordexp.len());
	let mut it = wordexp.iter();
	assert_eq!(Some("".as_ref()), it.next());
	assert_eq!(Some("ab".as_ref()), it.next());
	assert_eq!(None, it.next());
}

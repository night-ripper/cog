use std::io::Write;
use std::io::Read;
use raven::*;

fn main() -> std::process::ExitCode
{
	let asciidoctor = unsafe {libloading::Library::new("target/rust/x86_64-unknown-linux-gnu/debug/libasciidoctor.so").unwrap()};
	let addd = unsafe {asciidoctor.get::<fn(usize, usize) -> usize>(b"add").unwrap()};
	
	println!("add? {}", addd(5, 3));
	println!("name? {}", unsafe {asciidoctor.get::<*mut &str>(b"name").unwrap().as_ref().unwrap()});
	
	let mut args = std::env::args_os();
	let raven_bin_name = args.next().unwrap().into_string().unwrap();
	let args = match Arguments::new(args)
	{
		Ok(args) => {args}
		Err(error) =>
		{
			eprintln!("raven: {}", error);
			return 1.into();
		}
	};
	
	if args.help || std::env::args_os().skip(1).next().is_none()
	{
		print!("\
Usage: {} [options]... <targets>...
Options:
  -h, --help        Print help message.
  --clean           Clean all targets.

  -f <file>         Read the dependency graph from <file>
  -j <jobs>         Limit the number of concurrent jobs to <jobs>
", raven_bin_name);
		return std::process::ExitCode::SUCCESS;
	}
	
	if let Err(error) = std::env::set_current_dir(&args.current_directory)
	{
		eprintln!("raven: can not set current directory {:?}: {}", args.current_directory, error);
		return 1.into();
	}
	
	let mut filename = std::fs::File::open(args.filename.as_os_str()).unwrap();
	let mut string = String::with_capacity(128);
	filename.read_to_string(&mut string);
	let table = match toml::from_str::<toml::Table>(string.as_str())
	{
		Ok(table) => table,
		Err(error) =>
		{
			eprint!("raven: {}: {}", std::path::PathBuf::from(args.filename).canonicalize().unwrap().as_os_str().to_str().unwrap(), error);
			return 1.into();
		},
	};
	println!("{:?}", table);
	
	let mut rule_data = Vec::<Rule>::with_capacity(32);
	let mut rule_ids = std::collections::HashMap::<usize, usize>::with_capacity(32);
	let mut string_repository = Os_str_repository::with_capacity(32);
	
	if args.targets.is_empty()
	{
		eprintln!("raven: no target specified");
		return 1.into();
	}
	
	let mut stack = std::collections::VecDeque::with_capacity(32);
	
	for arg in args.targets.iter().rev()
	{
		let Some(target_index) = string_repository.find(std::ffi::OsStr::new(arg)) else
		{
			eprintln!("raven: no target {:?}", arg);
			return 1.into();
		};
		let target_index = string_repository[target_index].rule_id;
		if target_index == usize::MAX
		{
			eprintln!("raven: no rule to make target {:?}", arg);
			return 1.into();
		}
		stack.push_back(Stack_entry {index: target_index, kind: Stack_type::initial});
	}
	
	if args.graph
	{
		println!("digraph raven {{");
		println!("fontname = \"Monospace\"");
		println!("node [fontname = \"Monospace\", fontsize = 12, shape = box, height = 0.25, width = 0.25]");
		println!("edge [fontname = \"Monospace\", fontsize = 11]");
		println!("rankdir = \"LR\"");
	}
	
	let path_directories = std::env::var_os("PATH").unwrap_or_default().as_encoded_bytes().split(|&b| b == b':').map(
		|p| unsafe {std::ffi::OsStr::from_encoded_bytes_unchecked(p)}.to_os_string().into_boxed_os_str()
	).collect::<Vec<_>>();
	
	let mut jobs = std::collections::VecDeque::<usize>::with_capacity(32);
	
	//// DFS, but leave the leaf nodes at the beginning of the stack
	
	while let Some(target_entry) = stack.pop_back()
	{
		match target_entry.kind
		{
			Stack_type::initial =>
			{
				match rule_data[target_entry.index].dfs_state
				{
					Rule_dfs_state::initial =>
					{
						let target_rule = &mut rule_data[target_entry.index];
						stack.push_back(Stack_entry {index: target_entry.index, kind: Stack_type::finished});
						target_rule.dfs_state = Rule_dfs_state::discovered;
						
						rule_data[target_entry.index].is_leaf = true;
						
						let targets = std::mem::take(&mut rule_data[target_entry.index].targets);
						if rule_data[target_entry.index].implicit_directories.unwrap_or(true)
						{
							for &target_index in targets.as_slice()
							{
								let (kind, name) = string_repository.get(target_index as usize);
								if kind == Target_kind::file
								{
									let name = std::path::Path::new(name);
									if let Some(parent) = name.parent()
									{
										let mut parent = parent.as_os_str().to_os_string().into_encoded_bytes();
										parent.insert(0, b'd');
										let parent = unsafe {std::ffi::OsString::from_encoded_bytes_unchecked(parent)};
										string_repository.insert(parent, usize::MAX);
									}
								}
							}
						}
						
						if let Some(Recipe::command {command, args: _}) = &rule_data[target_entry.index].recipe
						{
							let (kind, executable) = string_repository.get(*command);
							if kind == Target_kind::executable
							{
								let mut path_name = std::path::PathBuf::with_capacity(32);
								for path in &path_directories
								{
									path_name.push(path.as_ref());
									path_name = path_name.join(executable);
									if let Ok(true) = path_name.try_exists()
									{
										let mut path_name = path_name.into_os_string().into_encoded_bytes();
										path_name.insert(0, b'.');
										let path_name = unsafe {std::ffi::OsString::from_encoded_bytes_unchecked(path_name)};
										string_repository.alias_name(path_name, *command);
										break;
									}
									path_name.clear();
								}
							}
							let (kind, executable) = string_repository.get(*command);
							if kind == Target_kind::executable
							{
								eprintln!("raven: could not find the executable {:?} on PATH {:?}", executable, path_directories);
								return 1.into();
							}
						}
						
						for &target_index in targets.as_slice()
						{
							let (kind, name) = string_repository.get(target_index as usize);
							if kind == Target_kind::file
							{
								match std::fs::metadata(name)
								{
									Ok(metadata) => match metadata.modified()
									{
										Ok(mtime) =>
										{
											string_repository[target_index as usize].mtime.insert(mtime);
										}
										Err(error) =>
										{
											eprintln!("raven: when obtaining modified time for target {:?}: {}", name, error);
											return 1.into();
										}
									}
									Err(error) if error.kind() == std::io::ErrorKind::NotFound =>
									{
										// ignore
									}
									Err(error) =>
									{
										eprintln!("raven: when obtaining metadata for target {:?}: {}", name, error);
										return 1.into();
									}
								};
							}
						}
						rule_data[target_entry.index].targets = targets;
						
						let sources = std::mem::take(&mut rule_data[target_entry.index].sources);
						for &source_index in sources.as_slice()
						{
							let rule_index = string_repository[source_index as usize].rule_id;
							if rule_index != usize::MAX
							{
								rule_data[target_entry.index].is_leaf = false;
								stack.push_back(Stack_entry {index: rule_index, kind: Stack_type::initial});
								rule_data[rule_index].dependants.push(target_entry.index as u32);
								continue;
							}
							
							rule_data[target_entry.index].sources_source += 1;
							
							if string_repository[source_index as usize].mtime.is_none()
							{
								let (kind, name) = string_repository.get(source_index as usize);
								if kind == Target_kind::file
								{
									match std::fs::metadata(name)
									{
										Ok(metadata) => match metadata.modified()
										{
											Ok(mtime) =>
											{
												string_repository[source_index as usize].mtime.insert(mtime);
											}
											Err(error) =>
											{
												eprintln!("raven: when obtaining modified time for source {:?} needed by {:?}: {}",
													name, rule_data[target_entry.index].targets.debug_names(&string_repository), error
												);
												return 1.into();
											}
										}
										Err(error) =>
										{
											eprintln!("raven: when obtaining metadata for source {:?} needed by {:?}: {}",
												name, rule_data[target_entry.index].targets.debug_names(&string_repository), error
											);
											return 1.into();
										}
									};
								}
							}
						}
						rule_data[target_entry.index].sources = sources;
					}
					Rule_dfs_state::discovered =>
					{
						let mut string = String::with_capacity(64);
						let mut it = stack.iter();
						
						for &stack_entry in &mut it
						{
							if let Stack_entry {index: stack_index, kind: Stack_type::finished} = stack_entry
							{
								if stack_index == target_entry.index
								{
									std::fmt::write(&mut string, format_args!("{:?}",
										rule_data[stack_index].targets.debug_names(&string_repository)
									)).unwrap();
									string.push_str(" -> ");
									break;
								}
							}
						}
						
						for &stack_entry in &mut it
						{
							if let Stack_entry {index: stack_index, kind: Stack_type::finished} = stack_entry
							{
								std::fmt::write(&mut string, format_args!("{:?}", 
									rule_data[stack_index].targets.debug_names(&string_repository)
								)).unwrap();
								string.push_str(" -> ");
							}
						}
						std::fmt::write(&mut string, format_args!("{:?}",
							rule_data[target_entry.index].targets.debug_names(&string_repository)
						)).unwrap();
						eprintln!("raven: cycle detected: {}", string);
						return 1.into();
					}
					Rule_dfs_state::finished => {}
				}
			}
			Stack_type::finished =>
			{
				let mut target_rule = std::mem::take(&mut rule_data[target_entry.index]);
				target_rule.dfs_state = Rule_dfs_state::finished;
				/*
				for &target_index in target_rule.targets.as_slice()
				{
					if string_repository.get(target_index as usize).0 == Target_kind::phony
					{
						for &source_index in target_rule.sources.as_slice()
						{
							let Some(source_mtime) = string_repository[source_index as usize].mtime else
							{
								string_repository[target_index as usize].mtime = None;
								break;
							};
							
							if let Some(mtime) = string_repository[target_index as usize].mtime.replace(source_mtime)
							{
								string_repository[target_index as usize].mtime.insert(mtime.max(source_mtime));
							}
						}
					}
				}
				*/
				if args.clean
				{
					for &clean_target in target_rule.targets.as_slice()
					{
						match string_repository.get(clean_target as usize)
						{
							(Target_kind::file, name) =>
							{
								match remove_file_or_dir(name)
								{
									Err(error) =>
									{
										eprintln!("raven: when cleaning up {:?}: {}", name, error);
										return 1.into();
									}
									Ok(true) =>
									{
										eprintln!("removed {:?}", name);
									}
									Ok(false) => {}
								}
							}
							_ => {}
						};
					}
				}
				else if args.graph
				{
					let mut stdout = std::io::stdout().lock();
					for &target in target_rule.targets.as_slice()
					{
						let (kind, target_name) = string_repository.get(target as usize);
						stdout.write(b"\"").unwrap();
						stdout.write(target_name.as_encoded_bytes()).unwrap();
						stdout.write(b"\"").unwrap();
						if kind == Target_kind::phony
						{
							stdout.write(b" [color = red]").unwrap();
						}
						writeln!(stdout).unwrap();
						
						if target_rule.sources.is_empty()
						{
							if let Some(recipe) = &target_rule.recipe
							{
								stdout.write(b"\"").unwrap();
								match recipe
								{
									Recipe::command {command, args: _} =>
									{
										stdout.write(string_repository.get(*command).1.as_encoded_bytes());
									}
									Recipe::script {..} =>
									{
										stdout.write(b"<script>").unwrap();
									}
									
								}
								stdout.write(b"\"").unwrap();
								stdout.write(b" -> \"").unwrap();
								stdout.write(target_name.as_encoded_bytes()).unwrap();
								stdout.write(b"\"").unwrap();
								// TODO
								// stdout.write(b" [shape = ellipse]").unwrap();
							}
							writeln!(stdout).unwrap();
						}
						
						for &source in target_rule.sources.as_slice()
						{
							// TODO
							let (_, source_name) = string_repository.get(source as usize);
							stdout.write(b"\"").unwrap();
							stdout.write(source_name.as_encoded_bytes()).unwrap();
							stdout.write(b"\" -> \"").unwrap();
							stdout.write(target_name.as_encoded_bytes()).unwrap();
							stdout.write(b"\"").unwrap();
							if let Some(recipe) = &target_rule.recipe
							{
								stdout.write(b" [label = \"").unwrap();
								match recipe
								{
									Recipe::command {command, args: _} =>
									{
										stdout.write(string_repository.get(*command).1.as_encoded_bytes());
									}
									Recipe::script {..} =>
									{
										stdout.write(b"<script>").unwrap();
									}
									
								}
								stdout.write(b"\"]").unwrap();
							}
							writeln!(stdout).unwrap();
						}
					}
				}
				else if target_rule.is_leaf
				{
					let min_target_mtime = target_rule.targets.as_slice().iter()
						.map(|&ti| string_repository[ti as usize].mtime).min_by(min_opt_mtime).unwrap()
					;
					let max_source_mtime = target_rule.sources.as_slice().iter()
						.map(|&si| string_repository[si as usize].mtime).max_by(
							|lhs, rhs| lhs.unwrap().cmp(&rhs.unwrap())
						)
					;
					
					match (min_target_mtime, max_source_mtime)
					{
						(Some(min_target_mtime), Some(Some(max_source_mtime))) if max_source_mtime <= min_target_mtime =>
						{
							for &dependant_index in target_rule.dependants.as_slice()
							{
								let dependant = &mut rule_data[dependant_index as usize];
								dependant.sources_finished += 1;
								if dependant.sources_finished
									== dependant.sources.as_slice().len() as u32 - dependant.sources_source
								{
									dependant.is_leaf = true;
								}
							}
							target_rule.was_already_done = true;
						}
						_ =>
						{
							jobs.push_back(target_entry.index);
						}
					};
				}
				rule_data[target_entry.index] = target_rule;
			}
		}
	}
	
	//// Create implicit directories
	for (key, _) in string_repository.mapping.range::<std::ffi::OsStr, (std::ops::Bound<&std::ffi::OsStr>, std::ops::Bound<&std::ffi::OsStr>)>((
		std::ops::Bound::Included(std::ffi::OsStr::new("d")), std::ops::Bound::Excluded(std::ffi::OsStr::new("e"))))
	{
		let directory = unsafe {std::ffi::OsStr::from_encoded_bytes_unchecked(&key.as_ref().as_encoded_bytes()[1 ..])};
		if let Err(error) = std::fs::create_dir_all(directory)
		{
			eprintln!("raven: when creating an implicit directory {:?}: {}", directory, error);
			return 1.into();
		}
	}
	
	/*
	for v in &string_repository.values
	{
		eprintln!("{:?}", v);
	}
	
	eprintln!("#####");
	
	for v in &stack
	{
		eprintln!("{:?}", v);
	}
	
	eprintln!("#####");
	
	for v in &rule_data
	{
		eprintln!("{:?}", v);
	}
	*/
	
	if args.graph
	{
		println!("}}");
		return std::process::ExitCode::SUCCESS;
	}
	
	for target in args.targets.iter()
	{
		let index = string_repository.find(target.as_ref()).unwrap();
		let index = string_repository[index].rule_id;
		if rule_data[index].was_already_done
		{
			eprintln!("raven: {:?} is up to date", target);
		}
	}
	
	let mut exitcode = 0;
	
	{
		struct Context
		{
			end: bool,
			jobs: std::collections::VecDeque<usize>,
			threads_remaining: usize,
		}
		
		let rule_data = std::sync::Mutex::new(rule_data);
		let condvar = std::sync::Condvar::new();
		let context = std::sync::Mutex::new(Context {jobs, end: false, threads_remaining: 0});
		
		std::thread::scope(|scope|
		{
			let mut threads = Vec::new();
			threads.reserve(std::thread::available_parallelism().unwrap_or(1.try_into().unwrap()).get());
			
			let capacity = threads.capacity();
			let rule_data = &rule_data;
			let context = &context;
			let condvar = &condvar;
			let string_repository = &string_repository;
			
			for _ in 0 .. capacity
			{
				threads.push(scope.spawn(move || -> Result<(), String>
				{
					loop
					{
						let mut guard = context.lock().unwrap();
						let rule_index;
						loop
						{
							if guard.end
							{
								return Ok(());
							}
							
							match guard.jobs.pop_front()
							{
								Some(index) =>
								{
									rule_index = index;
									break;
								}
								None =>
								{
									guard.threads_remaining += 1;
									if guard.threads_remaining == capacity
									{
										guard.end = true;
										condvar.notify_all();
										return Ok(());
									}
									guard = condvar.wait(guard).unwrap();
									if guard.end
									{
										return Ok(());
									}
									guard.threads_remaining -= 1;
								}
							}
						}
						drop(guard);
						
						let rule = std::mem::take(&mut rule_data.lock().unwrap()[rule_index]);
						
						if let Some(recipe) = rule.recipe.as_ref()
						{
							let mut command = match recipe
							{
								Recipe::command {command, args} =>
								{
									let mut stdout = std::io::stdout().lock();
									stdout.write(b"+ ").unwrap();
									let mut command = std::process::Command::new(string_repository.get(*command).1);
									stdout.write(command.get_program().as_encoded_bytes()).unwrap();
									for arg in args.iter()
									{
										stdout.write(b" ").unwrap();
										stdout.write(arg.as_bytes()).unwrap();
										command.arg(arg);
									}
									writeln!(stdout).unwrap();
									command
								}
								Recipe::script {script} =>
								{
									let mut command = std::process::Command::new("sh");
									command.arg("-ex");
									command.arg("-c");
									command.arg(script);
									command
								}
							};
							
							command.envs(rule.env.iter());
							command.stdout(std::process::Stdio::inherit());
							command.stderr(std::process::Stdio::inherit());
							
							if let Some(error) = match command.status()
							{
								Ok(status) if status.success() => {None}
								Ok(status) =>
								{
									if let Some(code) = status.code()
									{
										Some(format!("command {:?} failed with {}", command, code))
									}
									else
									{
										Some(format!("command {:?} failed", command))
									}
								}
								Err(error) =>
								{
									Some(format!("command {:?} failed to execute: {}", command, error))
								}
							}
							{
								// remove potentially incomplete target files
								for &target in rule.targets.as_slice()
								{
									let (kind, target) = string_repository.get(target as usize);
									if kind == Target_kind::file
									{
										// ignore errors
										if std::fs::remove_file(target).is_err()
										{
											std::fs::remove_dir_all(target);
										}
									}
								}
								let mut guard = context.lock().unwrap();
								guard.end = true;
								condvar.notify_all();
								return Err(error);
							}
						}
						
						{
							let mut rule_data = rule_data.lock().unwrap();
							let rule_data = &mut *rule_data;
							
							for &dependant_index in rule.dependants.as_slice()
							{
								let dependant = &mut rule_data[dependant_index as usize];
								dependant.sources_finished += 1;
								
								if dependant.sources_finished as usize
									== dependant.sources.as_slice().len() - dependant.sources_source as usize
								{
									let mut guard = context.lock().unwrap();
									guard.jobs.push_back(dependant_index as usize);
									condvar.notify_one();
								}
							}
							
							rule_data[rule_index] = rule;
						}
					}
				}));
			}
			
			for thread in threads.into_iter()
			{
				if let Err(error) = thread.join().unwrap()
				{
					exitcode = 1.into();
					eprintln!("raven: {}", error);
				}
			}
		});
	}
	
	return (exitcode as u8).into();
}

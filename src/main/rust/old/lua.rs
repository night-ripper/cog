use std::ffi::*;

pub type lua_KContext = c_long;
pub type lua_KFunction = unsafe extern "C" fn(lua: Lua_state, status: c_int, ctx: lua_KContext) -> c_int;
pub type lua_CFunction = unsafe extern "C" fn(lua: Lua_state) -> c_int;
pub type lua_Number = c_double;
pub type lua_Integer = c_longlong;
pub type lua_Unsigned = c_ulonglong;

pub type lua_Reader = unsafe extern "C" fn(lua: *mut c_void, user_data: *mut c_void, size: *mut usize) -> *const c_char;
pub type lua_Writer =  unsafe extern "C" fn(lua: *mut c_void, data: *const c_void, size: usize, user_data: *mut c_void) -> c_int;

pub const LUAI_MAXSTACK: c_int = 1000000;
pub const LUA_REGISTRYINDEX: c_int = -LUAI_MAXSTACK - 1000;
pub const LUA_IDSIZE: usize = 512;

pub const LUA_OK: c_int = 0;
pub const LUA_YIELD: c_int = 1;
pub const LUA_ERRRUN: c_int = 2;
pub const LUA_ERRSYNTAX: c_int = 3;
pub const LUA_ERRMEM: c_int = 4;
pub const LUA_ERRERR: c_int = 5;
pub const LUA_ERRFILE: c_int = LUA_ERRERR + 1;

// basic types
pub const LUA_TNONE: c_int = -1;
pub const LUA_TNIL: c_int = 0;
pub const LUA_TBOOLEAN: c_int = 1;
pub const LUA_TLIGHTUSERDATA: c_int = 2;
pub const LUA_TNUMBER: c_int = 3;
pub const LUA_TSTRING: c_int = 4;
pub const LUA_TTABLE: c_int = 5;
pub const LUA_TFUNCTION: c_int = 6;
pub const LUA_TUSERDATA: c_int = 7;
pub const LUA_TTHREAD: c_int = 8;

pub const LUA_MULTRET: c_int = -1;

#[repr(C)]
pub struct luaL_Reg
{
	pub name: *const c_char,
	pub func: Option<lua_CFunction>,
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct lua_Debug
{
	pub event: c_int,
	pub name: *const c_char, // (n)
	pub namewhat: *const c_char, // (n) 'global', 'local', 'field', 'method'
	pub what: *const c_char, // (S) 'Lua', 'C', 'main', 'tail'
	pub source: *const c_char, // (S)
	pub srclen: usize, // (S)
	pub currentline: c_int, // (l)
	pub linedefined: c_int, // (S)
	pub lastlinedefined: c_int, // (S)
	pub nups: c_uchar, // (u) number of upvalues
	pub nparams: c_uchar, // (u) number of parameters
	pub isvararg: c_char, // (u)
	pub istailcall: c_char, // (t)
	pub ftransfer: c_ushort, // (r) index of first value transferred
	pub ntransfer: c_ushort, // (r) number of transferred values
	pub short_src: [c_char; LUA_IDSIZE], // (S)
	// private part
	_i_ci: *mut c_void,
}

unsafe impl Send for lua_Debug {}
unsafe impl Sync for lua_Debug {}

impl Default for lua_Debug
{
	fn default() -> Self
	{
		unsafe {std::mem::zeroed()}
	}
}

pub const LUA_HOOKCALL: c_int = 0;
pub const LUA_HOOKRET: c_int = 1;
pub const LUA_HOOKLINE: c_int = 2;
pub const LUA_HOOKCOUNT: c_int = 3;
pub const LUA_HOOKTAILCALL: c_int = 4;

pub const LUA_MASKCALL: c_int = 1 << LUA_HOOKCALL;
pub const LUA_MASKRET: c_int = 1 << LUA_HOOKRET;
pub const LUA_MASKLINE: c_int = 1 << LUA_HOOKLINE;
pub const LUA_MASKCOUNT: c_int = 1 << LUA_HOOKCOUNT;

pub type lua_Hook = unsafe extern "C" fn(lua: *mut c_void, ar: *mut lua_Debug);

#[link(name = "lua")]
extern "C"
{
	pub fn lua_close(lua: *mut c_void);
	pub fn luaL_newstate() -> *mut c_void;
	pub fn luaL_openlibs(lua: *mut c_void);
	
	pub fn luaL_loadfilex(lua: *mut c_void, filename: *const c_char, mode: *const c_char) -> c_int;
	
	pub fn luaL_setfuncs(lua: *mut c_void, lib: *const luaL_Reg, nup: c_int);
	
	pub fn luaL_where(lua: *mut c_void, level: c_int);
	
	pub fn lua_getstack(lua: *mut c_void, level: c_int, activation_record: *mut lua_Debug) -> c_int;
	pub fn lua_getinfo(lua: *mut c_void, what: *const c_char, activation_record: *mut lua_Debug) -> c_int;
	
	// push functions (C -> stack)
	pub fn lua_pushnil(lua: *mut c_void);
	pub fn lua_pushnumber(lua: *mut c_void, n: lua_Number);
	pub fn lua_pushinteger(lua: *mut c_void, n: lua_Integer);
	pub fn lua_pushlstring(lua: *mut c_void, s: *const c_char, len: usize) -> *const c_char;
	pub fn lua_pushstring(lua: *mut c_void, s: *const c_char) -> *const c_char;
	pub fn lua_pushcclosure(lua: *mut c_void, func: lua_CFunction, num_upvalues: c_int);
	pub fn lua_pushboolean(lua: *mut c_void, b: c_int);
	pub fn lua_pushlightuserdata(lua: *mut c_void, p: *mut c_void);
	
	// get functions (Lua -> stack)
	pub fn lua_getglobal(lua: *mut c_void, name: *const c_char) -> c_int;
	pub fn lua_gettable(lua: *mut c_void, idx: c_int) -> c_int;
	pub fn lua_getfield(lua: *mut c_void, idx: c_int, k: *const c_char) -> c_int;
	pub fn lua_geti(lua: *mut c_void, idx: c_int, n: lua_Integer) -> c_int;
	pub fn lua_rawget(lua: *mut c_void, idx: c_int) -> c_int;
	pub fn lua_rawgeti(lua: *mut c_void, idx: c_int, n: lua_Integer) -> c_int;
	pub fn lua_rawgetp(lua: *mut c_void, idx: c_int, p: *const c_void) -> c_int;
	
	pub fn lua_createtable(lua: *mut c_void, narr: c_int, nrec: c_int);
	pub fn lua_newuserdatauv(lua: *mut c_void, sz: usize, nuvalue: c_int) -> *mut c_void;
	pub fn lua_getmetatable(lua: *mut c_void, objindex: c_int) -> c_int;
	pub fn lua_getiuservalue(lua: *mut c_void, idx: c_int, n: c_int) -> c_int;
	
	// set functions (stack -> Lua)
	pub fn lua_setglobal(lua: *mut c_void, name: *const c_char);
	pub fn lua_settable(lua: *mut c_void, idx: c_int);
	pub fn lua_setfield(lua: *mut c_void, idx: c_int, k: *const c_char);
	pub fn lua_seti(lua: *mut c_void, idx: c_int, n: lua_Integer);
	pub fn lua_rawset(lua: *mut c_void, idx: c_int);
	pub fn lua_rawseti(lua: *mut c_void, idx: c_int, n: lua_Integer);
	pub fn lua_rawsetp(lua: *mut c_void, idx: c_int, p: *const c_void);
	pub fn lua_setmetatable(lua: *mut c_void, objindex: c_int) -> c_int;
	pub fn lua_setiuservalue(lua: *mut c_void, idx: c_int, n: c_int) -> c_int;
	
	// 'load' and 'call' functions (load and run Lua code)
	pub fn lua_callk(lua: *mut c_void, nargs: c_int,
		nresults: c_int, ctx: lua_KContext, k: Option<lua_KFunction>,
	);
	pub fn lua_pcallk(lua: *mut c_void, nargs: c_int,
		nresults: c_int, errfunc: c_int, ctx: lua_KContext, k: Option<lua_KFunction>,
	) -> c_int;
	pub fn lua_load(lua: *mut c_void, reader: lua_Reader, user_data: *mut c_void, chunkname: *const c_char, mode: *const c_char) -> c_int;
	pub fn lua_dump(lua: *mut c_void, writer: lua_Writer, user_data: *mut c_void, strip: c_int) -> c_int;
	
	// coroutine functions
	pub fn lua_yieldk(lua: *mut c_void, nresults: c_int, ctx: lua_KContext, k: Option<lua_KFunction>) -> c_int;
	pub fn lua_resume(lua: *mut c_void, from: *mut c_void, narg: c_int, nres: *mut c_int) -> c_int;
	pub fn lua_status(lua: *mut c_void) -> c_int;
	pub fn lua_isyieldable(lua: *mut c_void) -> c_int;
	
	// basic stack manipulation
	pub fn lua_absindex(lua: *mut c_void, idx: c_int) -> c_int;
	pub fn lua_gettop(lua: *mut c_void) -> c_int;
	pub fn lua_settop(lua: *mut c_void, idx: c_int);
	pub fn lua_pushvalue(lua: *mut c_void, idx: c_int);
	pub fn lua_rotate(lua: *mut c_void, idx: c_int, n: c_int);
	pub fn lua_copy(lua: *mut c_void, fromidx: c_int, toidx: c_int);
	pub fn lua_checkstack(lua: *mut c_void, n: c_int) -> c_int;
	
	// access functions (stack -> C)
	pub fn lua_isnumber(lua: *mut c_void, idx: c_int) -> c_int;
	pub fn lua_isstring(lua: *mut c_void, idx: c_int) -> c_int;
	pub fn lua_iscfunction(lua: *mut c_void, idx: c_int) -> c_int;
	pub fn lua_isinteger(lua: *mut c_void, idx: c_int) -> c_int;
	pub fn lua_isuserdata(lua: *mut c_void, idx: c_int) -> c_int;
	pub fn lua_type(lua: *mut c_void, idx: c_int) -> c_int;
	pub fn lua_typename(lua: *mut c_void, tp: c_int) -> *const c_char;
	
	pub fn lua_tonumberx(lua: *mut c_void, idx: c_int, isnum: *mut c_int) -> lua_Number;
	pub fn lua_tointegerx(lua: *mut c_void, idx: c_int, isnum: *mut c_int) -> lua_Integer;
	pub fn lua_toboolean(lua: *mut c_void, idx: c_int) -> c_int;
	pub fn lua_tolstring(lua: *mut c_void, idx: c_int, len: *mut usize) -> *const c_char;
	pub fn lua_rawlen(lua: *mut c_void, idx: c_int) -> lua_Unsigned;
	pub fn lua_tocfunction(lua: *mut c_void, idx: c_int) -> lua_CFunction;
	pub fn lua_touserdata(lua: *mut c_void, idx: c_int) -> *mut c_void;
	pub fn lua_tothread(lua: *mut c_void, idx: c_int) -> *mut c_void;
	pub fn lua_topointer(lua: *mut c_void, idx: c_int) -> *const c_void;
	
	// miscellaneous functions
	pub fn lua_error(lua: *mut c_void) -> !;
	pub fn lua_next(lua: *mut c_void, idx: c_int) -> c_int;
	
	// debug functions
	pub fn lua_sethook(lua: *mut c_void, func: lua_Hook, mask: c_int, count: c_int);
	pub fn lua_gethook(lua: *mut c_void) -> Option<lua_Hook>;
	pub fn lua_gethookmask(lua: *mut c_void) -> c_int;
	pub fn lua_gethookcount(lua: *mut c_void) -> c_int;
}

pub const fn lua_upvalueindex(i: c_int) -> c_int
{
	LUA_REGISTRYINDEX - i
}

pub unsafe fn lua_pop(lua: *mut c_void, idx: c_int)
{
	lua_settop(lua, -idx - 1)
}

pub unsafe fn lua_replace(lua: *mut c_void, idx: c_int)
{
	lua_copy(lua, -1, idx);
	lua_pop(lua, 1)
}

pub unsafe fn lua_tointeger(lua: *mut c_void, idx: c_int) -> lua_Integer
{
	lua_tointegerx(lua, idx, std::ptr::null_mut())
}

pub unsafe fn lua_istable(lua: *mut c_void, idx: c_int) -> c_int
{
	(lua_type(lua, idx) == LUA_TTABLE).into()
}

pub unsafe fn lua_call(lua: *mut c_void, nargs: c_int, nresults: c_int)
{
	lua_callk(lua, nargs, nresults, 0, None)
}

pub unsafe fn lua_pcall(lua: *mut c_void, nargs: c_int, nresults: c_int, errfunc: c_int) ->c_int
{
	lua_pcallk(lua, nargs, nresults, errfunc, 0, None)
}

pub unsafe fn lua_yield(lua: *mut c_void, nresults: c_int) -> c_int
{
	lua_yieldk(lua, nresults, 0, None)
}

pub unsafe fn luaL_newlib(lua: *mut c_void, lib: &[luaL_Reg])
{
	// luaL_checkversion(L);
	lua_createtable(lua, 0, lib.len() as c_int - 1);
	luaL_setfuncs(lua, lib.as_ptr(), 0)
}

#[repr(transparent)]
#[derive(Clone, Copy)]
pub struct Lua_state
{
	pub object: std::ptr::NonNull<c_void>,
}

impl Lua_state
{
	pub fn tolstring(&mut self, idx: c_int) -> &[u8]
	{
		let mut len = 0;
		let string = unsafe {lua_tolstring(self.object.as_mut(), idx, &mut len)};
		unsafe {std::slice::from_raw_parts(string.cast(), len)}
	}
	
	pub fn pushlstring(&mut self, string: &[u8])
	{
		unsafe {lua_pushlstring(self.object.as_mut(), string.as_ptr().cast(), string.len())};
	}
}

#[repr(transparent)]
pub struct Lua(Lua_state);

impl Drop for Lua
{
	fn drop(&mut self)
	{
		unsafe {lua_close(self.object.as_mut())};
	}
}

impl Lua
{
	pub fn new() -> Self
	{
		let Some(object) = std::ptr::NonNull::new(unsafe {luaL_newstate()}) else
		{
			panic!("could not initialize Lua");
		};
		
		Self(Lua_state {object})
	}
}

impl std::ops::Deref for Lua
{
	type Target = Lua_state;
	
	fn deref(&self) -> &Self::Target {&self.0}
}

impl std::ops::DerefMut for Lua
{
	fn deref_mut(&mut self) -> &mut Self::Target {&mut self.0}
}

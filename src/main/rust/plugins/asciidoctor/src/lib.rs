#[derive(Debug, Default)]
struct Asciidoctor
{
	executable: std::ffi::OsString,
}

impl raven::plugin::Plugin for Asciidoctor
{
	
}

#[no_mangle]
fn new(arguments: &raven::plugin::Table) -> Box<dyn raven::plugin::Plugin>
{
	println!("{:?}", raven::which::which(std::ffi::OsStr::new("asciidoctor")));
	Box::new(Asciidoctor
	{
		// executable: raven::which::which(std::ffi::OsStr::new("asciidoctor")).unwrap().into()
		executable: Default::default(),
	})
}

pub struct Arguments
{
	pub targets: Vec<String>,
	pub filename: std::ffi::OsString,
	pub max_jobs: std::num::NonZeroUsize,
	pub current_directory: std::path::PathBuf,
	pub help: bool,
	pub clean: bool,
	pub graph: bool,
}

impl Default for Arguments
{
	fn default() -> Self
	{
		Self
		{
			max_jobs: std::thread::available_parallelism().unwrap_or(1.try_into().unwrap()),
			filename: "build.toml".into(),
			targets: Vec::with_capacity(8),
			current_directory: std::env::current_dir().unwrap(),
			help: false,
			clean: false,
			graph: false,
		}
	}
}

impl Arguments
{
	pub fn new(mut args: std::env::ArgsOs) -> Result<Self, String>
	{
		let mut result = Arguments::default();
		
		while let Some(mut arg) = args.next()
		{
			match arg.to_str().unwrap()
			{
				"-h" | "--help" =>
				{
					result.help = true;
				}
				"-f" | "--file" =>
				{
					let Some(next) = args.next() else
					{
						return Err(format!("missing file argument following -f"));
					};
					let mut next = next.into_encoded_bytes();
					next.push(b'\0');
					next.pop();
					unsafe {result.filename = std::ffi::OsString::from_encoded_bytes_unchecked(next)};
				}
				"-C" =>
				{
					let Some(curdir) = args.next() else
					{
						return Err(format!("missing file argument following -C"));
					};
					result.current_directory = result.current_directory.join(curdir.as_os_str());
				}
				"--clean" =>
				{
					result.clean = true;
				}
				arg_slice if arg_slice.starts_with("-j") =>
				{
					let start = if arg_slice.len() != 2 {2} else
					{
						let Some(next) = args.next() else
						{
							return Err(format!("missing max jobs argument following -j"));
						};
						arg = next;
						0
					};
					
					match arg.to_str().unwrap()[start ..].parse::<std::num::NonZeroUsize>()
					{
						Ok(parsed) => {result.max_jobs = parsed;}
						Err(error) => {return Err(format!("when parsing max jobs argument following -j: {}", error));}
					};
				}
				"--graph" =>
				{
					result.graph = true;
				}
				arg if arg.starts_with("-") =>
				{
					return Err(format!("unrecognized option: {:?}", arg));
				}
				arg =>
				{
					result.targets.push(arg.to_string());
				}
			}
		}
		
		if result.clean && result.graph
		{
			return Err(format!("options --clean and --graph can not be specified together"));
		}
		
		Ok(result)
	}
}
